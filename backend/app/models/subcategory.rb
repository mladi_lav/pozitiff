class Subcategory < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: :slugged
	belongs_to :category
	has_many :products

	def should_generate_new_friendly_id?
		(title.present? && slug.blank?) || super
	end
end
