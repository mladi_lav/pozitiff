class Page < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: :slugged
	has_attached_file :image
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
	attr_accessor :delete_image
	before_validation { self.image.clear if self.delete_image == '1' }

	def image_url
        ActionController::Base.asset_host + image.url()
    end

    has_attached_file :illustration
	validates_attachment_content_type :illustration, :content_type => /\Aimage\/.*\Z/
	attr_accessor :delete_illustration
	before_validation { self.illustration.clear if self.delete_illustration == '1' }

	def illustration_url
        ActionController::Base.asset_host + illustration.url()
    end

    def should_generate_new_friendly_id?
		(name.present? && slug.blank?) || super
	end
end
