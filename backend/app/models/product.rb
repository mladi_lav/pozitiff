class Product < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: :slugged
	belongs_to :subcategory
	belongs_to :category
	has_many :galleries, dependent: :destroy
	has_many :baskets
	has_attached_file :image
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
	attr_accessor :delete_image
	before_validation { self.image.clear if self.delete_image == '1' }

	scope :published, -> { where(public: true) }
	scope :in_category, lambda {|category| joins(:category).where('categories.slug = ?', category)}
	scope :in_subcategory, lambda {|subcategory| joins(:subcategory).where('subcategories.slug = ?', subcategory)}
	scope :by_ids, lambda {|ids| where(id: ids) }

	def image_url
        ActionController::Base.asset_host + image.url()
    end
	def next
		Product.published.order(:id).where('id > ?', id).where(:category_id => category_id).first
	end

	def prev
		Product.published.order(:id).where('id < ?', id).where(:category_id => category_id).last
	end

	def should_generate_new_friendly_id?
		(title.present? && slug.blank?) || super
	end
end
