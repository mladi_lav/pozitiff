class Delivery < ActiveRecord::Base
	has_attached_file :icon
	validates_attachment_content_type :icon, :content_type => /\Aimage\/.*\Z/
	attr_accessor :delete_icon
	before_validation { self.icon.clear if self.delete_icon == '1' }

	def image_url
        ActionController::Base.asset_host + icon.url()
    end
end
