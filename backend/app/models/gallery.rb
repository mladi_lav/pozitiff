class Gallery < ActiveRecord::Base
	belongs_to :product
	has_attached_file :image
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
	attr_accessor :delete_image
	before_validation { self.image.clear if self.delete_image == '1' }

	def url
        ActionController::Base.asset_host + image.url()
    end
end