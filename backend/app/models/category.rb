class Category < ActiveRecord::Base
	extend FriendlyId
	friendly_id :title, use: :slugged
	has_many :subcategories

	def should_generate_new_friendly_id?
		(title.present? && slug.blank?) || super
	end
end
