class Order < ActiveRecord::Base
  belongs_to :delivery
  belongs_to :payment
  has_many :baskets, dependent: :destroy
end
