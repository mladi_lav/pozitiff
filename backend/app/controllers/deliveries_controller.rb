class DeliveriesController < ApplicationController
	def index
		@deliveries = Delivery.all
		render json: {
			:result => @deliveries.to_json(:methods => [:image_url]),
			:status => 'success'
		}
	end
end