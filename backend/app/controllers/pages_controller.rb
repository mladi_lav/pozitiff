class PagesController < ApplicationController
	before_action :set_page, only: [:show]
	def show
		render json: {
			:result => @page.to_json(:methods => [:image_url, :illustration_url]),
			:status => 'success'
		}
	end

	def set_page
		@page = Page.friendly.find(params[:id])
	end
end