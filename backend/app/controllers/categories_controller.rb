class CategoriesController < ApplicationController
	before_action :set_subcategories, only: [:show]
	def index
		@categories = Category.all
		render json: {
			:result => @categories.to_json(:include => :category)
		}
	end

	def show
		render json: @category.to_json(:include => :subcategories)
	end

	private

	def set_subcategories
		@category = Category.friendly.find(params[:id])
	end
end