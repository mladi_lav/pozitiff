class OrdersController < ApplicationController
	skip_before_action :verify_authenticity_token

	def create
	  @order = Order.new(order_params)


	  if @order.save
	  	params[:basket].each do |n|
	  		@basket = Basket.new
	  		@basket.product_id = n[:product_id]
	  		@basket.order_id = @order.id
	  		@basket.count = n[:count]
	  		@basket.save
	  	end

	    render json: @order, status: :created
	  else
	    render json: @order, status: :unprocessable_entity
	  end
	end

	def order_params
	  params.require(:order).permit(:fio, :phone, :email, :delivery_id, :payment_id, :sum, :city, :address, :department)
	end
end