class ProductsController < ApplicationController
	before_action :set_product, only: [:show]
	def index
		if params[:subcategory] === 'all'
			@products = Product.published.in_category(params[:category]).page(params[:page]).per(params[:limit])
			@count = Product.published.in_category(params[:category]).count
		else
			@products = Product.published.in_subcategory(params[:subcategory])
			@count = Product.published.in_subcategory(params[:subcategory]).count
		end

		if params[:ids];
			@products = Product.published.by_ids(params[:ids].split(','))
			@count = @products.count
		end
		render json: {
			:products => @products.to_json(:methods => [:image_url], :include => [:subcategory, :category]),
			:count => @count
		}
	end

	def show
		render json: {
			:product => @product.to_json(:methods => [:image_url, :next, :prev], :include => [:subcategory, :category, :galleries]),
			:images => @product.galleries.as_json(:only => [:image], :methods => [:url])
		}
	end

	private

	def set_product
		@product = Product.published.friendly.find(params[:id])
	end
end