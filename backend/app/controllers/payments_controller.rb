class PaymentsController < ApplicationController
	def index
		@payments = Payment.all
		render json: {
			:result => @payments.to_json(:methods => [:image_url]),
			:status => 'success'
		}
	end
end