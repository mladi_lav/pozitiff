class SubcategoriesController < ApplicationController
	before_action :set_subcategories, only: [:show]
	def index
		@subcategories = Subcategory.all
		render json: {
			:result => @subcategories.to_json(:include => :category)
		}
	end

	def show
		render json: @subcategory
	end

	private

	def set_subcategories
		@subcategory = Subcategory.friendly.find(params[:id])
	end
end