working_directory "/var/www/pozitiff.in.ua/public_html/backend"
pid "/run/shm/unicorn-pozitiff.pid"
stderr_path "/var/log/unicorn/pozitiff/unicorn.log"
stdout_path "/var/log/unicorn/pozitiff/unicorn.log"

listen "/run/shm/unicorn-pozitiff.sock"
worker_processes 2
timeout 30
