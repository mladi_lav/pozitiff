RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
   config.authenticate_with do
     warden.authenticate! scope: :user
   end
   config.current_user_method(&:current_user)

  ## == Cancan ==
  # config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  ## == Gravatar integration ==
  ## To disable Gravatar integration in Navigation Bar set to false
  # config.show_gravatar = true

  config.model Subcategory do
    label 'Категория'
    label_plural 'Категории'
    nestable_list true
    exclude_fields :position, :slug, :products
    field :title do
      label 'Название'
      required true
    end
    field :category do
      label 'Раздел'
      required true
    end
  end

  config.model Category do
    label 'Раздел'
    label_plural 'Разделы'
    exclude_fields :slug
    field :title do
      label 'Название'
      required true
    end
    field :subcategories do
      label 'Категории'
    end
  end

  config.model Page do
    label 'Страница'
    label_plural 'Страницы'
    exclude_fields :slug
    field :name do
      label 'Название'
      required true
    end
    field :description, :ck_editor do
      label 'Описание'
      required true
    end
    field :image do
      label 'Картинка'
      required true
    end
    field :illustration do
      label 'Иллюстрация'
    end

    configure :image do
      pretty_value do
        bindings[:view].tag(:img, { :src => value, :height => '250px' }) 
      end
    end
    configure :illustration do
      pretty_value do
        bindings[:view].tag(:img, { :src => value, :height => '150px' }) 
      end
    end
  end

  config.model Order do
    label 'Заказ'
    label_plural 'Заказы'
    field :fio do
      label 'ФИО'
    end
    field :phone do
      label 'Телефон'
    end
    field :email do
      label 'Почта'
    end
    field :delivery do
      label 'Способ доставки'
    end
    field :payment do
      label 'Способ оплаты'
    end
    field :sum do
      label 'Сумма к оплате'
    end
    field :city do
      label 'Город'
    end
    field :address do
      label 'Адрес'
    end
    field :department do
      label 'Отделение новой почты'
    end
    field :baskets do
      label 'Корзина'
    end
  end

  config.model Basket do
    label 'Корзина'
    label_plural 'Корзины'
    field :product do
      label 'Продукт'
    end
    field :order do
      label 'Заказ'
    end
    field :count do
      label 'Количество'
    end
  end

  config.model Product do
    label 'Продукт'
    label_plural 'Продукты'
    nestable_list true
    include_fields :title, :public, :category, :subcategory, :image, :buy, :price, :delivery, :galleries
    edit do
      field :short_description, :ck_editor
      field :description, :ck_editor
      field :title do
        label 'Название'
        required true
      end

      field :public do
        label 'Отображение на сайте'
      end

      field :category do
        label 'Раздел'
        required true
      end

      field :subcategory do
        label 'Категория'
      end

      field :image do
        label 'Картинка'
        required true
      end

      field :buy do
        label 'Можно ли купить прямо на сайте?'
      end

      field :price do
        label 'Цена'
      end

      field :delivery do
        label 'Возможна ли доставка (на дом или новой почтой)'
      end

      field :galleries do
        label 'Картинки'
      end

      field :short_description do
        label 'Краткое описание'
        required true
      end

      field :description do
        label 'Полное описание'
        required true
      end

      configure :image do
        pretty_value do
          bindings[:view].tag(:img, { :src => value, :height => '250px' }) 
        end
      end
      
    end

    field :title do
      label 'Название'
    end

    field :public do
      label 'Отображение на сайте'
    end

    field :category do
      label 'Раздел'
    end

    field :subcategory do
      label 'Категория'
    end

    field :image do
      label 'Картинка'
    end

    field :buy do
      label 'Можно ли купить прямо на сайте?'
    end

    field :price do
      label 'Цена'
    end

    field :delivery do
      label 'Возможна ли доставка (на дом или новой почтой)'
    end

    field :galleries do
      label 'Картинки'
    end

    field :short_description do
      label 'Краткое описание'
    end

    field :description do
      label 'Полное описание'
    end


    configure :image do
      pretty_value do
        bindings[:view].tag(:img, { :src => value, :height => '250px' }) 
      end
    end

  end

  config.model Payment do
    label 'Способ оплаты'
    label_plural 'Способы оплаты'
    nestable_list true
    exclude_fields :position
    field :name do
      label 'Название'
      required true
    end
    field :description, :ck_editor do
      label 'Описание'
      required true
    end
    field :icon do
      label 'Иконка'
      required true
    end
  end

  config.model Delivery do
    label 'Способ доставки'
    label_plural 'Способы доставки'
    nestable_list true
    exclude_fields :position
    field :name do
      label 'Название'
      required true
    end
    field :description, :ck_editor do
      label 'Описание'
      required true
    end
    field :icon do
      label 'Иконка'
      required true
    end
    field :addres do
      label 'Нужно поле адреса при заказе'
    end
    field :link do
      label 'Нужно вставить ссылку на самовывоз'
    end
    field :postal do
      label 'Нужны поля для отправки почтой'
    end
  end

  config.model Gallery do
    label 'Картинка'
    label_plural 'Картинки'
    exclude_fields :product
  end

  config.model 'User' do
    visible false
  end

  config.model 'Ckeditor::Asset' do
    visible false
  end

  config.model 'Ckeditor::AttachmentFile' do
    visible false
  end

  config.model 'Ckeditor::Picture' do
    visible false
  end

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new do
      only [Gallery, Product, Subcategory]
    end
    bulk_delete
    show
    edit
    delete do
      only [Gallery, Product, Subcategory]
    end
    nestable

    ## With an audit adapter, you can add:
    # history_index
    # history_show
  end
end
