class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :title
      t.string :slug
      t.integer :position
      t.references :subcategory, index: true, foreign_key: true
      t.text :short_description
      t.text :description
      t.boolean :buy
      t.integer :price
      t.boolean :delivery
      t.boolean :public

      t.timestamps null: false
    end
  end
end
