class CreateBaskets < ActiveRecord::Migration
  def change
    create_table :baskets do |t|
      t.references :product, index: true, foreign_key: true
      t.references :order, index: true, foreign_key: true
      t.integer :count

      t.timestamps null: false
    end
  end
end
