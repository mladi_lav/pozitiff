class AddPostalToDeliveries < ActiveRecord::Migration
  def change
    add_column :deliveries, :postal, :boolean
  end
end
