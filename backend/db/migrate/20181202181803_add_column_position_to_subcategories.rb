class AddColumnPositionToSubcategories < ActiveRecord::Migration
  def change
    add_column :subcategories, :position, :integer
  end
end
