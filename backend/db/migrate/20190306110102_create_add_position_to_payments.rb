class CreateAddPositionToPayments < ActiveRecord::Migration
  def change
    add_column :payments, :position, :integer
  end
end
