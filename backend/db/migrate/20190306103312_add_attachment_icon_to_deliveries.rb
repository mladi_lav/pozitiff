class AddAttachmentIconToDeliveries < ActiveRecord::Migration
  def self.up
    change_table :deliveries do |t|
      t.attachment :icon
    end
  end

  def self.down
    remove_attachment :deliveries, :icon
  end
end
