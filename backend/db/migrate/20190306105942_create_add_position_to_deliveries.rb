class CreateAddPositionToDeliveries < ActiveRecord::Migration
  def change
    add_column :deliveries, :position, :integer
  end
end
