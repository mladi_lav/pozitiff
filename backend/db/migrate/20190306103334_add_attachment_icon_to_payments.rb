class AddAttachmentIconToPayments < ActiveRecord::Migration
  def self.up
    change_table :payments do |t|
      t.attachment :icon
    end
  end

  def self.down
    remove_attachment :payments, :icon
  end
end
