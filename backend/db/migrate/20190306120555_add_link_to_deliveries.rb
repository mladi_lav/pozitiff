class AddLinkToDeliveries < ActiveRecord::Migration
  def change
    add_column :deliveries, :link, :boolean
  end
end
