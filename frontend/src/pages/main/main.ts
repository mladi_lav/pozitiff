import { Component, AfterViewInit } from '@angular/core';
import { environment } from '../../environments/environments';
import * as anime from 'animejs';

@Component({
  selector: 'page-main',
  templateUrl: 'main.html'
})
export class MainPage implements AfterViewInit {

	public env = environment;
	constructor() {
	}

	ngAfterViewInit(): void {
		this.animate();
	}

	private animate() {
      let animate = anime.timeline();
      let animateBalloons = anime.timeline();

      animate.add(this.animateLogo())
      .add(this.animateScale());

      animateBalloons.add(this.animateDownBallons())
      .add(this.animateUpBallons());
  	}

  	private animateDownBallons() {
  		return {
          targets: ['.balloon-1', '.balloon-3'],
          opacity: 1,
		  translateY: [
		  	{value: '-100%', duration: 0},
		  	{value: '15%', duration: 2000}
		  ],
		  delay: (el, i, l) => {
            return (i * 300);
          }
        }
  	}

  	private animateUpBallons() {
  		return {
          targets: ['.balloons', '.balloon-2'],
          opacity: 1,
		  translateY: [
		  	{value: '200%', duration: 0},
		  	{value: '35%', duration: 2000}
		  ],
		  delay: (el, i, l) => {
            return (i * 300);
          }
        }
  	}

  	private animateLogo() {
  		return {
          targets: '.main-logo-animate',
          delay:1000,
		  scale: [
		  	{value: 0, duration: 0},
		  	{value: 1, duration: 3000}
		  ]
        }
  	}

  	private animateScale() {
  		return {
          targets: ['.main-phones-animate', '.main-email-animate', '.footer-item', '.map', '.copyrights'],
		  scale: [
		  	{value: 0, duration: 0},
		  	{value: 1, duration: 1000}
		  ],
		  delay: (el, i, l) => {
            return (i * 300);
          }
        }
  	}

}
