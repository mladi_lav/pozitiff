export class PageModel {
    public name: string;
    public description: string;
    public slug: string;
    public id: string;
    public image_url: string;
    public illustration_url?: string;
}
