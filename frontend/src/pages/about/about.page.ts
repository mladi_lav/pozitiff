import { Component } from '@angular/core';
import { PageModel } from '../page.model';
import { BackendService } from '../../shared/backend/backend.service';

@Component({
  selector: 'page-about',
  templateUrl: 'about.page.html'
})
export class AboutPage {
  public page: PageModel;
  constructor(private backend: BackendService) {
  	this.backend.getPage('about').subscribe(resp => {
  		this.page = JSON.parse(resp["result"]);
  	});
  }

}
