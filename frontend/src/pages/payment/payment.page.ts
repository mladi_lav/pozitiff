import { Component } from '@angular/core';
import { BackendService } from '../../shared/backend/backend.service';
import { PageModel } from '../page.model';
import { PaymentModel } from '../order/order.model';


@Component({
  selector: 'page-payment',
  templateUrl: 'payment.page.html'
})

export class PaymentPage {
  public payments: PaymentModel[] = [];
  public page: PageModel;
  constructor(private backend: BackendService) {
  	this.backend.getPayments().subscribe(resp => {
  		this.payments = JSON.parse(resp["result"]);
  	});

  	this.backend.getPage('payment').subscribe(resp => {
  		this.page = JSON.parse(resp["result"]);
  	});
  }

}
