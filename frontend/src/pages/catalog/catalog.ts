import { Component, ViewChild, OnDestroy, ElementRef } from '@angular/core';
import { NavParams, Events} from 'ionic-angular';
import { ProductModel } from '../../shared/catalog/product/product.model';
import { CategoryThemeModel, CurrentTheme, CategoryModel } from '../../shared/catalog/category/category.model';
import { Slides } from 'ionic-angular';
import { NavService } from '../../shared/nav/nav.service';
import { EventTopic } from '../../shared/event/event.enum';
import { BackendService } from '../../shared/backend/backend.service';
import * as anime from 'animejs';

@Component({
  selector: 'catalog',
  templateUrl: 'catalog.html'
})

export class CatalogPage implements OnDestroy{
  @ViewChild(Slides) slides: Slides;
  public currentTheme: CategoryThemeModel;
  public limit: number = 4;
  public page: number = 1;
  public totalCount: number = 0;
  public products: ProductModel[] = [];
  public category: string;
  public subcategory: string;
  public isOpenCategory: boolean =  false;
  public categories: CategoryModel[] = [];
  public title: string = "";

  private animate: anime = null;

  constructor(private navParams: NavParams,
              private nav: NavService,
              private events: Events,
              private backend: BackendService,
              protected elementRef: ElementRef) {

    console.log('this.navParams', this.navParams);
    this.category = this.navParams.get('category');
    this.subcategory = this.navParams.get('subcategory');
    this.currentTheme = CurrentTheme.getTheme(this.category);
    this.resizeSlider();

    this.events.subscribe(EventTopic.OpenProduct, slug => {
      this.openProduct(slug);
    });

    this.backend.getCategory(this.category).subscribe(category => {
        this.categories = category["subcategories"];
        this.title = category["title"];
    });

    this.backend.getProducts(this.category, this.subcategory, this.limit, this.page).subscribe(result => {
      this.products = JSON.parse(result['products']);
      this.totalCount = result['count'];
    });
  }

  ngOnDestroy(): void {
    this.events.unsubscribe(EventTopic.OpenProduct);
  }

  ionViewDidEnter() {
    if(this.animate) {
      this.animate.reverse();
      this.animate.play();
    }
  }

  resizeSlider(): void  {
    let width = window.innerWidth;
    if(width <= 700) {
      this.limit = 1;
      return;
    }
    if(width <= 800) {
      this.limit = 2;
      return;
    }

    if(width <= 1920) {
      this.limit = 3;
      return;
    }
  }

  openProduct(slug): void  {
    let el = this.elementRef.nativeElement;
    this.animate =  anime.timeline();
    this.animate.add(this.animateTitle(el))
    .add(this.animateCategory(el))
    .add(this.animateCatalog(el, slug));
  }

  toogleCategory(): void  {
    this.isOpenCategory = !this.isOpenCategory;
  }

  changeCategory(subcategory: CategoryModel): void  {
    this.nav.goTo('catalog', {category: this.category, subcategory: subcategory.slug});
  }

  next(): void  {
    this.slides.slideNext();
  }

  prev(): void  {
    this.slides.slidePrev();
  }

  public onSlideDidChange(event) {
    if(event.isEnd() && this.showLoader()) {
      this.page++;
      this.backend.getProducts(this.category, this.subcategory, this.limit, this.page).subscribe(result => {
        this.products = this.products.concat(JSON.parse(result['products']));
      });


    }
  }

  public showLoader():boolean {
    return !(this.products.length == this.totalCount);
  }

  private animateTitle(el) {
    return {
      targets: [el.querySelector('h1')],
      duration:1000,
      opacity: 0,
      translateY: '-100%',
    }
  }

  private animateCategory(el) {
    return {
      targets: [el.querySelector('h1')],
      duration:1000,
      opacity:0,
      translateX: '100%',
    }
  }

  private animateCatalog(el, slug: string) {
    return {
      targets: [
        el.querySelector('.catalog'),
        el.querySelector('.general-img')
      ],
      duration:1000,
      opacity: 0,
      translateY: '100%',
      delay: (elem, i, l) => {
        return (i * 200);
      },
      complete: (el) => {
        if(slug !== '')
          this.nav.goTo('product', {slug: slug});
      }
    }
  }
}
