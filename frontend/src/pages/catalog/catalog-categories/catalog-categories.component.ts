import { Component, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { NavService } from '../../../shared/nav/nav.service';
import { CategoryModel } from '../../../shared/catalog/category/category.model';
import { Slides, NavParams } from 'ionic-angular';

@Component({
  selector: 'catalog-categories',
  templateUrl: 'catalog-categories.component.html'
})

export class CatalogCategoriesComponent {
  @ViewChild(Slides) slides: Slides;

  @Input('color') public color: string = 'yellow';
  @Input('categories') public categories: CategoryModel[] = [];
  @Input('isOpen') public isOpen: boolean = false;
  @Output('openCategory')
  public openCategory: EventEmitter<CategoryModel>;
  constructor(private nav: NavService, private navParams: NavParams) {
    this.openCategory = new EventEmitter<CategoryModel>();
  }


  goTo(subcategory) {
    this.openCategory.emit(subcategory);
  }
  next() {
    this.slides.slideNext();
  }
  prev() {
    this.slides.slidePrev();
  }
}






