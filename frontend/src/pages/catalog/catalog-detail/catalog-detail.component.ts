import { Component, ViewChild } from '@angular/core';
import { NavParams, Slides } from 'ionic-angular';
import { CategoryThemeModel, CurrentTheme } from '../../../shared/catalog/category/category.model';
import { ProductDetailModel } from '../../../shared/catalog/product/product.model';
import { NavService } from '../../../shared/nav/nav.service';
import { BackendService } from '../../../shared/backend/backend.service';


@Component({
  selector: 'catalog-detail',
  templateUrl: 'catalog-detail.component.html'
})

export class CatalogDetailComponent {
  @ViewChild(Slides) slides: Slides;
  public currentTheme: CategoryThemeModel;
  public isProductLoaded: boolean = false;
  public product: ProductDetailModel = new ProductDetailModel();

  constructor(private navParams: NavParams,
              private nav: NavService,
              private backend: BackendService) {
    this.backend.getProduct(this.navParams.get('slug')).subscribe(result => {
      let product = JSON.parse(result["product"]);
      this.currentTheme = CurrentTheme.getTheme(product.category.slug);
      this.product = product;
      this.product.images = [];
      this.product.images.push({url: this.product.image_url});
      this.product.images = this.product.images.concat(result["images"]);
      this.isProductLoaded = true;
    })
  }

  openProduct(slug: string) {
    this.nav.goTo('product', {slug: slug});
  }

  back() {
    this.nav.goTo('catalog', {category: this.product.category.slug, subcategory: "all"});
  }
}
