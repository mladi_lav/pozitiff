import { Component, Input } from '@angular/core';
import { ProductModel } from '../../../shared/catalog/product/product.model';
import { EventTopic } from '../../../shared/event/event.enum';
import { Events } from 'ionic-angular';

@Component({
  selector: 'catalog-item',
  templateUrl: 'catalog-item.component.html'
})

export class CatalogItemComponent {

  @Input('color')
  	public color: string = 'yellow';

  @Input('product')
  	public product: ProductModel;

  constructor(private events: Events) {}

  openProduct(slug: string) {
  	this.events.publish(EventTopic.OpenProduct, slug);
  }
}






