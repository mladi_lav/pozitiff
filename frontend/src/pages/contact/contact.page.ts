import { Component } from '@angular/core';
import { environment } from '../../environments/environments';
import { PageModel } from '../page.model';
import { BackendService } from '../../shared/backend/backend.service';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.page.html'
})
export class ContactPage {
  public env = environment;
  public page: PageModel;
  constructor(private backend: BackendService) {
  	this.backend.getPage('contact').subscribe(resp => {
  		this.page = JSON.parse(resp["result"]);
  	});
  }
}
