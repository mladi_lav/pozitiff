import { Component } from '@angular/core';
import { environment } from '../../environments/environments';
import { BackendService } from '../../shared/backend/backend.service';
import { DeliveryModel } from '../order/order.model';
import { PageModel } from '../page.model';

@Component({
  selector: 'page-delivery',
  templateUrl: 'delivery.page.html'
})
export class DeliveryPage {
  public env = environment;
  public deliveries: DeliveryModel[] = [];
  public page: PageModel;

  constructor(private backend: BackendService) {
  	this.backend.getDeliveries().subscribe(resp => {
  		this.deliveries = JSON.parse(resp["result"]);
  	});
  	this.backend.getPage('delivery').subscribe(resp => {
  		this.page = JSON.parse(resp["result"]);
  	});
  }
}
