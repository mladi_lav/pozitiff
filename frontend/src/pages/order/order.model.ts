export class OrderModel {
    public email: string = "";
    public fio: string = "";
    public phone: string = "";
    public delivery?: DeliveryModel;
    public deliveryAddress: DeliveryAddressModel = new DeliveryAddressModel();
    public payment?: PaymentModel;
    public products?: number[];
    public sum?: number;
}

export class DeliveryModel {
	public id: number;
	public name: string;
	public description: string;
	public addres?: boolean;
	public postal?: boolean;
	public link?: boolean;
	public image_url: string;
}

export class PaymentModel {
	public id: number;
	public name: string;
	public description: string;
	public image_url: string;
}

export class DeliveryAddressModel {
	public city?: string = "";
	public department?: string = "";
	public address?: string = "";
}
