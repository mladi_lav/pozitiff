import { Component } from '@angular/core';
import { ModalController } from 'ionic-angular';
import { FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { OrderModel, DeliveryModel, PaymentModel } from "./order.model";
import { environment } from "../../environments/environments";
import { ToastController } from 'ionic-angular';
import { BasketModalComponent } from '../../shared/basket/basket-modal/basket-modal.component';
import { UserService } from '../../shared/user/user.service';
import { BasketService } from '../../shared/basket/basket.service';
import { BackendService } from '../../shared/backend/backend.service';

@Component({
  selector: 'page-order',
  templateUrl: 'order.page.html'
})
export class OrderPage {
	public env = environment;
	public sum: number = 0;
	public orderForm: FormGroup;
	public userOrder: OrderModel = new OrderModel();
	public deliveries: DeliveryModel[] = [];
	public payments: PaymentModel[] = [];
	public isFormLoaded: boolean = false;

	constructor(private toastCtrl: ToastController,
				private formBuilder: FormBuilder,
				private userService: UserService,
				private basket: BasketService,
				private backend: BackendService,
				private modalCtrl: ModalController) {
		this.userOrder = this.userService.get();
		this.backend.getDeliveries().subscribe(resp => {
  			this.deliveries = JSON.parse(resp["result"]);
  			if(!this.userOrder.delivery) {
  				this.userOrder.delivery = this.deliveries[0];
  			}
  			this.backend.getPayments().subscribe(resp => {
	  			this.payments = JSON.parse(resp["result"]);
	  			if(!this.userOrder.payment) {
	  				this.userOrder.payment = this.deliveries[0];
	  			}
	  			this.buildForm(this.userOrder);
	  			this.isFormLoaded = true;
		  	});
	  	});

	}

	public onSubmit() {
		if(this.orderForm.invalid) {
			this.presentToast('Пожалуйста заполните все поля!');
			return;
		}
		this.showConfirmModal();
	}

	public getDelivery() {
		return this.deliveries.filter(delivery => delivery.id === this.orderForm.controls.delivery.value)[0];
	}

	public changeDelivery() {
		let delivery: DeliveryModel =  this.getDelivery();
		if(delivery.postal && !this.basket.enableDelivery()) {
			this.presentToast('Данный тип доставки невозможен, так как в корзине присутствуют товары, которые не доставляются новой почтой.');
			this.orderForm.controls.delivery.setValue(2);
		}
		this.updateForm(delivery);
	}

	private showConfirmModal() {
		let confirmModal = this.modalCtrl.create(BasketModalComponent, this.orderForm.value);
		confirmModal.present();
		confirmModal.onDidDismiss(confirm => {
			if(!confirm) return;
			//this.sum = 0;
		})
	}

	private buildForm(order: OrderModel) {
		this.orderForm = this.formBuilder.group(this.getBasicForm(order));
		this.updateForm(this.getDelivery());
	}

	private updateForm(delivery: DeliveryModel) {
		if(delivery.postal === true) {
			this.orderForm.removeControl('address');
			this.orderForm.addControl('city',
				new FormControl(this.userOrder.deliveryAddress.city, Validators.required));
			this.orderForm.addControl('department',
				new FormControl(this.userOrder.deliveryAddress.department, Validators.required));
			return;
		}
		if(delivery.addres === true) {
			this.orderForm.removeControl('department');
			this.orderForm.addControl('city',
				new FormControl(this.userOrder.deliveryAddress.city, Validators.required));
			this.orderForm.addControl('address',
				new FormControl(this.userOrder.deliveryAddress.address, Validators.required));
			return;
		}

		this.orderForm.removeControl('department');
		this.orderForm.removeControl('address');
		this.orderForm.removeControl('city');
	}

	private getBasicForm(order: OrderModel) {
		return {
			email: [
				order.email,
				[Validators.required, Validators.email]
			],
			fio: [
				order.fio,
				Validators.required
			],
			phone: [
				order.phone,
				Validators.required
			],
			delivery: [
				order.delivery.id,
				Validators.required
			],
			payment: [
				order.payment.id,
				Validators.required
			]
		}
	}

	private presentToast(message) {
		let toast = this.toastCtrl.create({
			message: message,
			duration: 3000,
			position: 'bottom'
		});
		toast.present();
	}
}
