import { CategoryModel } from "../category/category.model";
export class ProductModel {
    public id: number;
    public title: string = "";
    public slug: string = "";
    public category: CategoryModel;
    public category_id: number;
    public subcategory_id: number;
    public image_url: string = "";
    public short_description: string = "";
    public buy: boolean;
    public price: number;
    public delivery: boolean;
    public sum?: number;
    public count?: number;

}

export class ProductDetailModel extends ProductModel{
	public description: string;
    public subcategory: CategoryModel;
	public images?: any[];
}
