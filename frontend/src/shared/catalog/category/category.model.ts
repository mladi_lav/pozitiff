
export class CategoryThemes {
	public static flowers: CategoryThemeModel = {
		color: "blue",
		theme: "flowers"
	};
	public static balloons: CategoryThemeModel = {
		color: "yellow",
		theme: "balloons"
	};
	public static decor: CategoryThemeModel = {
		color: "blue",
		theme: "balloons"
	};
	public static gifts: CategoryThemeModel = {
		color: "yellow",
		theme: "gifts"
	};
}
export class CategoryModel {
    public position?: number;
    public title: string;
    public slug: string;
    public id: string;
}
export class CategoryThemeModel {
	public color: string = 'yellow';
	public theme: string = 'balloons';
}

export class CurrentTheme {
	public static getTheme(category):CategoryThemeModel {
		return {
			color: CategoryThemes[category].color,
		  	theme: CategoryThemes[category].theme
		}
	}
}