import {Injectable} from "@angular/core";
import {OrderModel} from "../../pages/order/order.model";

@Injectable()
export class UserService {
  constructor() {
  }
  public save(order: OrderModel) {
    localStorage.setItem('user', JSON.stringify(order));
  }

  public get(): OrderModel {
    let order: OrderModel = null;
    order = JSON.parse(localStorage.getItem('user'));
    return order === undefined || order === null ? new OrderModel : order;
  }
}