export enum EventTopic {
	OpenProduct = "product:open",
	BasketCleaned = "basket:cleaned"
}