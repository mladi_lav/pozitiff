import {Pipe, PipeTransform} from "@angular/core";
import {CurrencyPipe} from "@angular/common";

@Pipe({
    name: "currency",
    pure: false
})
export class PricePipe implements PipeTransform {

    constructor() {}

    transform(value: any, options?: { currencyCode?: string, symbolDisplay?: boolean, digits?: string }): string {

        return value + ' грн.';
    }
}
