import {Injectable} from "@angular/core";
import {Headers, Http, Request, RequestMethod, RequestOptions, RequestOptionsArgs, Response} from "@angular/http";
import {Observable} from "rxjs/Rx";
import {ProductModel} from '../catalog/product/product.model';
import {OrderModel} from '../../pages/order/order.model';
import {environment} from '../../environments/environments';

@Injectable()
export class BackendService {
  protected timeoutInMillisSec = 30000;
  protected headers: Headers = new Headers();

  constructor(private http: Http) {
    this.headers.append('Accept', 'application/json');
  }

  public getProducts(category: string, subcategory: string, limit: number, page: number):Observable<Response> {
    let url = '/products?category='
              + category + '&subcategory='
              + subcategory + '&limit='
              + limit + '&page='
              + page;
    return this.request(url, null, RequestMethod.Get);
  }

  public getProductsByIds(ids):Observable<Response> {
    let url = '/products?ids=' + ids;
    return this.request(url, null, RequestMethod.Get);
  }

  public getProduct(slug: string):Observable<Response>  {
    return this.request('/products/' + slug, null, RequestMethod.Get);
  }

  public getPage(slug: string):Observable<Response>  {
    return this.request('/pages/' + slug, null, RequestMethod.Get);
  }

  public getCategory(code:string):Observable<Response> {
    return this.request('/categories/' + code, null, RequestMethod.Get);
  }

  public getDeliveries():Observable<Response>  {
    return this.request('/deliveries/', null, RequestMethod.Get);
  }

  public getPayments():Observable<Response>  {
    return this.request('/payments/', null, RequestMethod.Get);
  }


  public sendOrder(order: OrderModel, basket:any, sum: number):Observable<Response>  {
    let basketResult = [];
    let data = {
      fio: order.fio,
      phone: order.phone,
      email: order.email,
      payment_id: order.payment.id,
      delivery_id: order.delivery.id,
      sum: sum,
      address: "",
      department: "",
      city:""
    }

    Object.keys(basket).forEach(key => {
      basketResult.push({product_id: key, count: basket[key]})
    });

    if(order.deliveryAddress) {
      if(order.deliveryAddress.address) {
        data['address'] = order.deliveryAddress.address;
      }

      if(order.deliveryAddress.city) {
        data['city'] = order.deliveryAddress.city;
      }

      if(order.deliveryAddress.department) {
        data['department'] = order.deliveryAddress.department;
      }
    }
    return this.request('/orders/',{order: data,  basket: basketResult}, RequestMethod.Post);
  }

  private request(url: string,
                   data: any,
                   method: RequestMethod,
                   handleError: boolean = true,
                   params?: string | URLSearchParams | { [key: string]: any | any[]; } | null): Observable<Response> {

        const requestOptionsArgs: RequestOptionsArgs = {
            url: environment.api + url,
            method: method,
            headers: this.headers,
            body: data,
            params: params,
        };

        const requestOptions: RequestOptions = new RequestOptions(requestOptionsArgs);

        return this.http.request(new Request(requestOptions))
            .timeout(this.timeoutInMillisSec)
            .catch(error => {
                return Observable.throw(error);
            }).map(response => response.json());
    }

}