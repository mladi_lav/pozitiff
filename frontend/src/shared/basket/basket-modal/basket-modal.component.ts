import { Component } from '@angular/core';
import { BasketService } from '../basket.service';
import { NavService } from '../../nav/nav.service';
import { NavParams, ViewController } from 'ionic-angular';
import { ProductModel } from '../../catalog/product/product.model';
import { UserService } from '../../user/user.service';
import { BackendService } from '../../backend/backend.service';
import { DeliveryModel, PaymentModel, OrderModel } from '../../../pages/order/order.model';

@Component({
  selector: 'basket-modal',
  templateUrl: 'basket-modal.component.html'
})

export class BasketModalComponent {

	public products: ProductModel[] = [];
	public basketProducts: any;
	public sum: number = 0;
	public delivery: DeliveryModel;
  public payment: PaymentModel;
	public isSend: boolean = false;
  public user: OrderModel = new OrderModel();

  constructor(public params: NavParams,
              private backend: BackendService,
  			      private basket: BasketService,
              private viewCtrl: ViewController,
              private userService: UserService,
  			      private nav: NavService) {

      this.basket.getProducts().subscribe(products => {
        this.products = products;
        this.sum = this.basket.getSum(this.products);
      });
  		this.basketProducts = this.basket.get();
      this.backend.getDeliveries().subscribe(resp => {
        let deliveries: DeliveryModel[] = JSON.parse(resp["result"]);
        this.delivery = deliveries.filter(item => item.id === this.params.get('delivery'))[0];
      });

      this.backend.getPayments().subscribe(resp => {
        let payments: PaymentModel[] = JSON.parse(resp["result"]);
        this.payment = payments.filter(item => item.id === this.params.get('payment'))[0];
      });
  }

  public close() {
    this.viewCtrl.dismiss(true);
  }

  public onSubmit() {
    this.basket.clear();
    this.saveUser();
  }

  private saveUser() {
    this.user.fio = this.params.get('fio');
    this.user.email = this.params.get('email');
    this.user.phone = this.params.get('phone');
    this.user.delivery = this.delivery;
    this.user.payment = this.payment;
    this.user.deliveryAddress.city = this.params.get('city');
    this.user.deliveryAddress.address = this.params.get('address');
    this.user.deliveryAddress.department = this.params.get('department');
    this.backend.sendOrder(this.user, this.basketProducts, this.sum).subscribe(resp => {
      this.userService.save(this.user);
      this.isSend = true;
    });
  }
}
