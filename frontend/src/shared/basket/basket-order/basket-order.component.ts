import { Component, Input, ViewChild, Output, EventEmitter, OnDestroy } from '@angular/core';
import { BasketService } from '../basket.service';
import { Slides, Events } from 'ionic-angular';
import { ProductModel } from '../../catalog/product/product.model';
import { MenuItemsForOrder } from '../../menu/menu.data';
import { MenuModel } from '../../menu/menu.model';
import { NavService } from '../../nav/nav.service';
import { EventTopic } from '../../event/event.enum';

@Component({
  selector: 'basket-order',
  templateUrl: 'basket-order.component.html'
})

export class BasketOrderComponent implements OnDestroy {
  @ViewChild('Slides') slides: Slides;
  @Input('sum')
  public sum: number = 0;
  @Output()
  public sumChange = new EventEmitter<number>();

  public products: ProductModel[] = [];
  public basketProducts:any = null;
  public count: number = 3;
  public menuItems: MenuModel[] = MenuItemsForOrder;

  constructor(public nav: NavService,
              public events: Events,
              public basket: BasketService) {

  	this.basketProducts = this.basket.get();
    this.basket.getProducts().subscribe(products => {
      this.products = products;
      this.updateSum();
    });

    this.events.subscribe(EventTopic.BasketCleaned, () => {
      this.basket.getProducts().subscribe(products => {
        this.products = products;
        this.updateSum();
      });
    });
  }

  ngOnDestroy(): void {
    this.events.unsubscribe(EventTopic.BasketCleaned);
  }

  public getCount(): number {
  	return this.basket.getCount();
  }

  public decrement(id: number) {
  	this.basketProducts[id]--;
  	if(this.basketProducts[id] === 0) {
  		this.basketProducts[id] = 1;
  	}
  	this.save();
  }

  public increment(id: number) {
  	this.basketProducts[id]++;
  	this.save();
  }

  public remove(index: number) {
  	delete this.basketProducts[this.products[index].id];
  	this.products.splice(index, 1);
  	if(this.products.length === 1) {
  		this.slides.slideTo(0);
  	}
  	this.save();
  }

  public next() {
    this.slides.slideNext();
  }

  public prev() {
    this.slides.slidePrev();
  }

  public goTo(page, category) {
    this.nav.goTo(page, {category: category, subcategory: 'all'});
  }

  private save() {
  	this.basket.set(this.basketProducts);
  	this.updateSum();
  }

  private updateSum() {
  	this.sum = this.basket.getSum(this.products);
    this.sumChange.emit(this.sum);
  }
  private resizeSlider(): void  {
    let width = window.innerWidth;
    if(width <= 1024) {
      this.count = 1;
      return;
    }
  }
}
