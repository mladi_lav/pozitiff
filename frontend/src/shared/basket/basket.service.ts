import { Injectable } from "@angular/core";
import { ToastController, Events } from 'ionic-angular';
import { ProductModel } from '../catalog/product/product.model';
import { BackendService } from '../../shared/backend/backend.service';
import { EventTopic } from '../../shared/event/event.enum';
import { Observable } from "rxjs/Rx";

@Injectable()
export class BasketService {
  private products: ProductModel[] = [];

  constructor(private toastCtrl: ToastController, private backend: BackendService, private events: Events) {
    this.getProducts().subscribe(products => {
      this.products = products;
    });
  }

  public getProducts(): Observable<ProductModel[]> {
    return new  Observable<ProductModel[]>(observer => {
      let basket = this.get();
      let ids = Object.keys(basket);
      this.backend.getProductsByIds(ids).subscribe(resp => {
         observer.next(JSON.parse(resp["products"]));
      });
    });
  }

  public add(id: number) {
    let items = this.get();
    if(items.hasOwnProperty(id)) {
      items[id] = items[id] + 1;
    } else {
      items[id] = 1;
    }
    this.set(items);
    this.presentToast('Товар успешно добавлен в корзину!')
  }

  public getCount():number {
    let items = this.get();
    let count:number = 0;
    for (let i in items) {
      count += items[i];
    }
    return count;
  }

  public get() {
    return JSON.parse(localStorage.getItem('basket')) || {};
  }

  public set(items) {
    localStorage.setItem('basket', JSON.stringify(items));;
  }


  public getSum(products: ProductModel[] ):number {
    let sum = 0;
    let basket = this.get();

    if(basket.length === 0) {
      products = [];
      return sum;
    }

    products.map(product => {
      sum += product.price * basket[product.id];
    });
    return sum;
  }

  public clear() {
    this.set({});
    this.events.publish(EventTopic.BasketCleaned);
  }

  public enableDelivery():boolean {
    let isEnable:boolean = true;

    this.products.map(product => {
      if(product.delivery === false) {
        isEnable = false;
        return;
      }
    });
    return isEnable;
  }

  private presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }
}