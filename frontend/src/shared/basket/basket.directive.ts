import {Directive, ElementRef, Input} from "@angular/core";
import {BasketService} from "./basket.service";

@Directive({
    selector: "[add-to-basket]",
    host: {
        "(click)": "onClick($event)"
    }
})

export class BasketDirective {
    @Input('add-to-basket')
    public id: number = null;

    constructor(protected elementRef: ElementRef, private basket: BasketService) {
    }

    onClick(event) {
    	this.basket.add(this.id);
    }

}
