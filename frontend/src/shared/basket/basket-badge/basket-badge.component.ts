import { Component } from '@angular/core';
import { BasketService } from '../basket.service';
import { NavService } from '../../nav/nav.service';

@Component({
  selector: 'basket-badge',
  templateUrl: 'basket-badge.component.html'
})

export class BasketBadgeComponent {


  constructor(private basket: BasketService, private nav: NavService) {}

  public getCount(): number {
  	return this.basket.getCount();
  }
  public openOrder() {
  	this.nav.goTo('order', {});
  }
}
