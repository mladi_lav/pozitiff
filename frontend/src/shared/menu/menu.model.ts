export class MenuModel {
    public name: string;
    public color: string;
    public page: string;
    public category: string;
    public icon: string;
}
