import { Component } from '@angular/core';
import { NavService } from '../../shared/nav/nav.service';
import { MenuModel } from './menu.model';
import { MenuItems } from './menu.data';
import { NavHeaderService } from '../../shared/nav/nav-header/nav-header.service';
import { environment } from '../../environments/environments';

@Component({
  selector: 'aside-menu',
  templateUrl: 'menu.component.html'
})
export class MenuComponent {
  public menuItems: MenuModel[] = MenuItems;
  public env = environment;
  constructor(public nav: NavService, public navHeader: NavHeaderService) {
  }

  toogleMenu() {
  	this.navHeader.toogleMenu();
  }
  goTo(page, category) {
  	this.navHeader.toogleMenu();
  	this.nav.goTo(page, {category: category, subcategory: 'all'});
  }

}
