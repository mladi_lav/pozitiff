import { CategoryThemes } from '../../shared/catalog/category/category.model';
import { MenuModel } from './menu.model';


export const MenuItems: MenuModel[] = [
  		{
  			name: "О нас",
    		color: "yellow",
    		page: "about",
    		category: null,
    		icon: "about"
  		},
  		{
  			name: "Букеты",
    		color: CategoryThemes.flowers.color,
    		page: "catalog",
    		category: "flowers",
    		icon: "flowers"
  		},
  		{
  			name: "Шарики",
    		color: CategoryThemes.balloons.color,
    		page: "catalog",
    		category: "balloons",
    		icon: "balloons"
  		},
  		{
  			name: "Оформление",
    		color: CategoryThemes.decor.color,
    		page: "catalog",
    		category: "decor",
    		icon: "decor"
  		},
  		{
  			name: "Подарки",
    		color: CategoryThemes.gifts.color,
    		page: "catalog",
    		category: "gifts",
    		icon: "presents"
  		},
  		{
  			name: "Доставка",
    		color: "blue",
    		page: "delivery",
    		category: null,
    		icon: "delivery"
  		},
  		{
  			name: "Оплата",
    		color: "yellow",
    		page: "payment",
    		category: null,
    		icon: "payment"
  		},
  		{
  			name: "Контакты",
    		color: "blue",
    		page: "contacts",
    		category: null,
    		icon: "contacts"
  		}
  	];


export const MenuItemsForOrder: MenuModel[] = [
      {
        name: "Букеты",
        color: CategoryThemes.flowers.color,
        page: "catalog",
        category: "flowers",
        icon: "flowers"
      },
      {
        name: "Шарики",
        color: CategoryThemes.balloons.color,
        page: "catalog",
        category: "balloons",
        icon: "balloons"
      },
      {
        name: "Оформление",
        color: CategoryThemes.decor.color,
        page: "catalog",
        category: "decor",
        icon: "decor"
      },
      {
        name: "Подарки",
        color: CategoryThemes.gifts.color,
        page: "catalog",
        category: "gifts",
        icon: "presents"
      }
    ];