import {OnInit, Directive, ElementRef} from "@angular/core";
import * as anime from 'animejs';

@Directive({
    selector: "[animate-image]",
    exportAs: "animate-image"
})

export class AnimateImageDirective implements OnInit {
    private duration: number = 3000;
    constructor(protected elementRef: ElementRef) {
    }

    ngOnInit(): void {
      //this.animate();
    }

    private animate() {
      anime({
        targets: this.elementRef.nativeElement,
        opacity: 1,
        translateX:[
          { value: '100%', duration: 0 },
          { value: 0, duration: this.duration }
        ],
        easing: 'easeOutExpo'
      });
    }

}
