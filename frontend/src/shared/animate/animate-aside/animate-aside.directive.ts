import {OnInit, Directive, ElementRef, ContentChild} from "@angular/core";
import * as anime from 'animejs';

@Directive({
    selector: "[animate-aside]",
    exportAs: "animate-aside"
})

export class AnimateAsideDirective implements OnInit {
    @ContentChild('illustartion') illustartion;
    @ContentChild('list') list;
    @ContentChild('title') title;
    @ContentChild('description') description;
    private duration: number = 600;
    constructor(protected elementRef: ElementRef) {
    }

    ngOnInit(): void {
      //this.animate();
    }

    private animate() {
      let animate = anime.timeline();
      animate.add(this.aside());
      //.add(this.animateText());

      if(this.list) {
        //animate.add(this.animateList());
      }

      if(this.illustartion) {
        //animate.add(this.animateIllustration());
      }
    }

    private animateIllustration() {
      let illustration = this.illustartion.nativeElement;
      return {
          targets: illustration,
          opacity: 1,
          translateY:[
            { value: '100%', duration: 0 },
            { value: 0, duration: 3*this.duration }
          ],
          easing: 'easeOutExpo'
      }
    }
    private animateList() {
      let list = this.list.nativeElement.querySelectorAll('.static-page__list-item');
      return {
          targets: list,
          opacity:1,
          easing: 'easeOutExpo',
          translateX: [
            { value: '100%', duration: 0 },
            { value: 0, duration: this.duration }
          ],
          duration:this.duration,
          delay: (el, i, l) => {
            return (i * this.duration);
          }
      }
    }

    private animateText() {
      return {
          targets: [
              this.title.nativeElement,
              this.description.nativeElement
              ],
          opacity: 1,
          translateX:[
            { value: '100%', duration: 0 },
            { value: 0, duration: this.duration }
          ],
          easing: 'easeOutExpo',
          delay: (el, i, l) => {
            return (i * this.duration);
          }
      }
    }

    private aside() {
      return {
          targets: this.elementRef.nativeElement,
          easing: 'linear',
          translateX: [
            { value: '100%', duration: 0 },
            { value: 0, duration: this.duration }
          ]
        }
    }


}
