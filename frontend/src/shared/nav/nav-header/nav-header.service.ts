import {Injectable} from "@angular/core";

@Injectable()
export class NavHeaderService {

  public menuOpen: boolean = false;
  constructor() {
  }
  toogleMenu() {
  	this.menuOpen = !this.menuOpen;
  }
}