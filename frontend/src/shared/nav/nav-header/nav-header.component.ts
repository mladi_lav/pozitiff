import { Component } from '@angular/core';
import { NavHeaderService } from './nav-header.service';
import { NavService } from '../nav.service';

@Component({
  selector: 'nav-header',
  templateUrl: 'nav-header.component.html'
})
export class NavHeaderComponent {

  constructor(public navHeader: NavHeaderService, public nav: NavService) {
  }
  goHome() {
  	if(this.navHeader.menuOpen === true){
  		this.navHeader.toogleMenu();
  	}
  	this.nav.goHome();
  }

}
