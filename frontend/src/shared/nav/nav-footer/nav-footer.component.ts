import { Component } from '@angular/core';
import { NavService } from '../nav.service';
import { environment } from '../../../environments/environments';

@Component({
  selector: 'nav-footer',
  templateUrl: 'nav-footer.component.html'
})
export class NavFooterComponent {
  public env = environment;
  constructor(public nav: NavService) {
  }

}
