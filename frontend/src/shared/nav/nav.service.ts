import {Injectable} from "@angular/core";
import {Nav} from "ionic-angular";

@Injectable()
export class NavService {
  public nav: Nav;
  constructor() {
  }
  public goTo(page, params) {
  	this.nav.push(page, params, {animate: false});
  }

  public goHome() {
    let first = this.nav.first().name;
    if(first !== 'MainPage') {
  	  this.nav.setRoot('main');
    } else {
      this.nav.popToRoot();
    }
  }
  public ready(nav: Nav): void {
        this.nav = nav;
   }
}