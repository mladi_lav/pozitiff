import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from "@angular/http";
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { MainPage } from '../pages/main/main';

import { CatalogPage } from '../pages/catalog/catalog';
import { CatalogCategoriesComponent } from '../pages/catalog/catalog-categories/catalog-categories.component';
import { CatalogItemComponent } from '../pages/catalog/catalog-item/catalog-item.component';
import { CatalogDetailComponent } from '../pages/catalog/catalog-detail/catalog-detail.component';

import { MenuComponent } from '../shared/menu/menu.component';
import { NavService } from '../shared/nav/nav.service';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { NavHeaderComponent } from '../shared/nav/nav-header/nav-header.component';
import { NavFooterComponent } from '../shared/nav/nav-footer/nav-footer.component';
import { NavHeaderService } from '../shared/nav/nav-header/nav-header.service';


import { AboutPage } from '../pages/about/about.page';
import { ContactPage } from '../pages/contact/contact.page';
import { DeliveryPage } from '../pages/delivery/delivery.page';
import { PaymentPage } from '../pages/payment/payment.page';

import { AnimateAsideDirective } from '../shared/animate/animate-aside/animate-aside.directive';
import { AnimateImageDirective } from '../shared/animate/animate-image/animate-image.directive';

import { BasketDirective } from '../shared/basket/basket.directive';
import { BasketBadgeComponent } from '../shared/basket/basket-badge/basket-badge.component';
import { BasketOrderComponent } from '../shared/basket/basket-order/basket-order.component';
import { BasketService } from '../shared/basket/basket.service';
import { BasketModalComponent } from '../shared/basket/basket-modal/basket-modal.component';

import { PricePipe } from '../shared/price/price.pipe';
import { OrderPage } from '../pages/order/order.page';
import { UserService } from '../shared/user/user.service';
import { BackendService } from '../shared/backend/backend.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { TooltipsModule } from 'ionic-tooltips';


@NgModule({
  declarations: [
    MyApp,
    MainPage,
    CatalogPage,
    OrderPage,
    CatalogCategoriesComponent,
    CatalogItemComponent,
    CatalogDetailComponent,
    MenuComponent,
    NavHeaderComponent,
    NavFooterComponent,
    BasketBadgeComponent,
    BasketOrderComponent,
    BasketModalComponent,
    AboutPage,
    ContactPage,
    DeliveryPage,
    PaymentPage,

    AnimateAsideDirective,
    AnimateImageDirective,

    BasketDirective,

    PricePipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    TooltipsModule.forRoot(),
    IonicModule.forRoot(MyApp, {}, {
     links: [
      { component: AboutPage, name: 'about', segment: 'about' },
      { component: ContactPage, name: 'contacts', segment: 'contacts' },
      { component: DeliveryPage, name: 'delivery', segment: 'delivery' },
      { component: PaymentPage, name: 'payment', segment: 'payment' },
      { component: MainPage, name: 'main', segment: 'main' },
      { component: CatalogPage, name: 'catalog', segment: 'catalog/:category/:subcategory' },
      { component: CatalogDetailComponent, name: 'product', segment: 'product/:slug' },
      { component: OrderPage, name: 'order', segment: 'order' }
    ]
  })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MainPage,
    CatalogPage,
    OrderPage,
    CatalogCategoriesComponent,
    CatalogItemComponent,
    CatalogDetailComponent,
    MenuComponent,
    NavHeaderComponent,
    NavFooterComponent,
    AboutPage,
    ContactPage,
    DeliveryPage,
    PaymentPage,
    BasketBadgeComponent,
    BasketModalComponent,
    BasketOrderComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    NavService,
    NavHeaderService,
    UserService,
    PricePipe,
    BasketService,
    BackendService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
