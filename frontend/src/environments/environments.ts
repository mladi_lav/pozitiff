export const environment = {
	api: "https://api.pozitiff.in.ua",
	address:  {
		title: "Ирпень, ул. Славянская, 35",
		link: "https://www.google.com/maps/place/%D0%93%D1%83%D0%BB%D0%B8%D0%B2%D0%B5%D1%80/@50.5204642,30.2403301,16.97z/data=!4m13!1m7!3m6!1s0x472b33d30f42e483:0x398df907db8e4e6a!2z0YPQuy4g0KHQu9Cw0LLRj9C90YHQutCw0Y8sIDM1LCDQmNGA0L_QtdC90YwsINCa0LjQtdCy0YHQutCw0Y8g0L7QsdC70LDRgdGC0YwsIDA4MjAw!3b1!8m2!3d50.5204981!4d30.2423805!3m4!1s0x472b33d30f10640b:0xcbe8005ffa3b63b6!8m2!3d50.5205802!4d30.2422929"
	},
	email: "hello@pozitiff.in.ua",
	phones: [
		{
			title: "+38 066 45 38 367",
			link: "tel:+380664538367"
		},
		{
			title: "+38 067 64 71 467",
			link: "tel:+380676471467"
		}
	],
	socials: {
		fb: "https://www.facebook.com/groups/446618002569718/",
		instagram: "https://www.instagram.com/prazdnik.pozitiv.irpen/"
	},
	reference: "https://mrgnc.io/"
}