webpackJsonp([0],{

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryThemes; });
/* unused harmony export CategoryModel */
/* unused harmony export CategoryThemeModel */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return CurrentTheme; });
var CategoryThemes = /** @class */ (function () {
    function CategoryThemes() {
    }
    CategoryThemes.flowers = {
        color: "blue",
        theme: "flowers"
    };
    CategoryThemes.balloons = {
        color: "yellow",
        theme: "balloons"
    };
    CategoryThemes.decor = {
        color: "blue",
        theme: "balloons"
    };
    CategoryThemes.gifts = {
        color: "yellow",
        theme: "gifts"
    };
    return CategoryThemes;
}());

var CategoryModel = /** @class */ (function () {
    function CategoryModel() {
    }
    return CategoryModel;
}());

var CategoryThemeModel = /** @class */ (function () {
    function CategoryThemeModel() {
        this.color = 'yellow';
        this.theme = 'balloons';
    }
    return CategoryThemeModel;
}());

var CurrentTheme = /** @class */ (function () {
    function CurrentTheme() {
    }
    CurrentTheme.getTheme = function (category) {
        return {
            color: CategoryThemes[category].color,
            theme: CategoryThemes[category].theme
        };
    };
    return CurrentTheme;
}());

//# sourceMappingURL=category.model.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavHeaderService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavHeaderService = /** @class */ (function () {
    function NavHeaderService() {
        this.menuOpen = false;
    }
    NavHeaderService.prototype.toogleMenu = function () {
        this.menuOpen = !this.menuOpen;
    };
    NavHeaderService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], NavHeaderService);
    return NavHeaderService;
}());

//# sourceMappingURL=nav-header.service.js.map

/***/ }),

/***/ 155:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_order_order_model__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserService = /** @class */ (function () {
    function UserService() {
    }
    UserService.prototype.save = function (order) {
        localStorage.setItem('user', JSON.stringify(order));
    };
    UserService.prototype.get = function () {
        var order = null;
        order = JSON.parse(localStorage.getItem('user'));
        return order === undefined || order === null ? new __WEBPACK_IMPORTED_MODULE_1__pages_order_order_model__["a" /* OrderModel */] : order;
    };
    UserService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], UserService);
    return UserService;
}());

//# sourceMappingURL=user.service.js.map

/***/ }),

/***/ 156:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderModel; });
/* unused harmony export DeliveryModel */
/* unused harmony export PaymentModel */
/* unused harmony export DeliveryAddressModel */
var OrderModel = /** @class */ (function () {
    function OrderModel() {
        this.email = "";
        this.fio = "";
        this.phone = "";
        this.deliveryAddress = new DeliveryAddressModel();
    }
    return OrderModel;
}());

var DeliveryModel = /** @class */ (function () {
    function DeliveryModel() {
    }
    return DeliveryModel;
}());

var PaymentModel = /** @class */ (function () {
    function PaymentModel() {
    }
    return PaymentModel;
}());

var DeliveryAddressModel = /** @class */ (function () {
    function DeliveryAddressModel() {
        this.city = "";
        this.department = "";
        this.address = "";
    }
    return DeliveryAddressModel;
}());

//# sourceMappingURL=order.model.js.map

/***/ }),

/***/ 170:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 170;

/***/ }),

/***/ 216:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 216;

/***/ }),

/***/ 262:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environments__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_animejs__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_animejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_animejs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var MainPage = /** @class */ (function () {
    function MainPage() {
        this.env = __WEBPACK_IMPORTED_MODULE_1__environments_environments__["a" /* environment */];
    }
    MainPage.prototype.ngAfterViewInit = function () {
        this.animate();
    };
    MainPage.prototype.animate = function () {
        var animate = __WEBPACK_IMPORTED_MODULE_2_animejs__["timeline"]();
        var animateBalloons = __WEBPACK_IMPORTED_MODULE_2_animejs__["timeline"]();
        animate.add(this.animateLogo())
            .add(this.animateScale());
        animateBalloons.add(this.animateDownBallons())
            .add(this.animateUpBallons());
    };
    MainPage.prototype.animateDownBallons = function () {
        return {
            targets: ['.balloon-1', '.balloon-3'],
            opacity: 1,
            translateY: [
                { value: '-100%', duration: 0 },
                { value: '15%', duration: 2000 }
            ],
            delay: function (el, i, l) {
                return (i * 300);
            }
        };
    };
    MainPage.prototype.animateUpBallons = function () {
        return {
            targets: ['.balloons', '.balloon-2'],
            opacity: 1,
            translateY: [
                { value: '200%', duration: 0 },
                { value: '35%', duration: 2000 }
            ],
            delay: function (el, i, l) {
                return (i * 300);
            }
        };
    };
    MainPage.prototype.animateLogo = function () {
        return {
            targets: '.main-logo-animate',
            delay: 1000,
            scale: [
                { value: 0, duration: 0 },
                { value: 1, duration: 3000 }
            ]
        };
    };
    MainPage.prototype.animateScale = function () {
        return {
            targets: ['.main-phones-animate', '.main-email-animate', '.footer-item', '.map', '.copyrights'],
            scale: [
                { value: 0, duration: 0 },
                { value: 1, duration: 1000 }
            ],
            delay: function (el, i, l) {
                return (i * 300);
            }
        };
    };
    MainPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-main',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/main/main.html"*/'<ion-content class="main-page" no-bounce>\n	<nav-header></nav-header>\n	<div class="wrap">\n		<div class="info">\n			<div class="logo main-logo-animate">\n				<img src="assets/images/logo.png">\n			</div>\n			<div class="phones main-phones-animate">\n				<a *ngFor="let phone of env.phones" [href]="phone.link">{{phone.title}}</a>\n			</div>\n			<a href="mailto:{{env.email}}" class="main-email-animate">{{env.email}}</a>\n\n			<div class="map">\n				<div class="map-title">Мы всегда рады видеть вас в нашем магазине</div>\n				<img src="assets/images/icons/label.png">\n				<div><a [href]="env.address.link">{{env.address.title}}</a></div>\n			</div>\n		</div>\n		<div class="footer">\n			<div class="footer-item">\n				<div class="footer-icon">\n					<img src="assets/images/icons/delivery3.svg">\n				</div>\n				<div class="footer-info">\n					<div class="footer-title">Наш адрес</div>\n					Мы всегда рады видеть вас в нашем магазине <br>\n					<a [href]="env.address.link">{{env.address.title}}</a>\n				</div>\n			</div>\n			<div class="footer-item">\n				<div class="footer-icon">\n					<img src="assets/images/icons/share.svg">\n				</div>\n				<div class="footer-info">\n					<div class="footer-title">Мы в социальных сетях</div>\n					Следите за новыми работами и скидками\n				</div>\n			</div>\n\n			<div class="footer-item footer-item--social">\n				<a [href]="env.socials.fb" class="footer-icon">\n					<img src="assets/images/icons/fb.svg">\n				</a>\n			</div>\n			<div class="footer-item footer-item--social">\n				<a [href]="env.socials.instagram" class="footer-icon">\n					<img src="assets/images/icons/instagram.svg">\n				</a>\n			</div>\n		</div>\n\n		<img class="balloons" src="assets/images/bg/balloon4.svg">\n		<img class="balloon-1" src="assets/images/bg/balloon1.png">\n		<img class="balloon-2" src="assets/images/bg/balloon2.svg">\n		<img class="balloon-3" src="assets/images/bg/balloon3.svg">\n		<nav-footer></nav-footer>\n	</div>\n</ion-content>\n'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/main/main.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], MainPage);
    return MainPage;
}());

//# sourceMappingURL=main.js.map

/***/ }),

/***/ 27:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavService = /** @class */ (function () {
    function NavService() {
    }
    NavService.prototype.goTo = function (page, params) {
        this.nav.push(page, params, { animate: false });
    };
    NavService.prototype.goHome = function () {
        var first = this.nav.first().name;
        if (first !== 'MainPage') {
            this.nav.setRoot('main');
        }
        else {
            this.nav.popToRoot();
        }
    };
    NavService.prototype.ready = function (nav) {
        this.nav = nav;
    };
    NavService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [])
    ], NavService);
    return NavService;
}());

//# sourceMappingURL=nav.service.js.map

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BackendService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environments__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var BackendService = /** @class */ (function () {
    function BackendService(http) {
        this.http = http;
        this.timeoutInMillisSec = 30000;
        this.headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]();
        this.headers.append('Accept', 'application/json');
    }
    BackendService.prototype.getProducts = function (category, subcategory, limit, page) {
        var url = '/products?category='
            + category + '&subcategory='
            + subcategory + '&limit='
            + limit + '&page='
            + page;
        return this.request(url, null, __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestMethod */].Get);
    };
    BackendService.prototype.getProductsByIds = function (ids) {
        var url = '/products?ids=' + ids;
        return this.request(url, null, __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestMethod */].Get);
    };
    BackendService.prototype.getProduct = function (slug) {
        return this.request('/products/' + slug, null, __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestMethod */].Get);
    };
    BackendService.prototype.getPage = function (slug) {
        return this.request('/pages/' + slug, null, __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestMethod */].Get);
    };
    BackendService.prototype.getCategory = function (code) {
        return this.request('/categories/' + code, null, __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestMethod */].Get);
    };
    BackendService.prototype.getDeliveries = function () {
        return this.request('/deliveries/', null, __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestMethod */].Get);
    };
    BackendService.prototype.getPayments = function () {
        return this.request('/payments/', null, __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestMethod */].Get);
    };
    BackendService.prototype.sendOrder = function (order, basket, sum) {
        var basketResult = [];
        var data = {
            fio: order.fio,
            phone: order.phone,
            email: order.email,
            payment_id: order.payment.id,
            delivery_id: order.delivery.id,
            sum: sum,
            address: "",
            department: "",
            city: ""
        };
        Object.keys(basket).forEach(function (key) {
            basketResult.push({ product_id: key, count: basket[key] });
        });
        if (order.deliveryAddress) {
            if (order.deliveryAddress.address) {
                data['address'] = order.deliveryAddress.address;
            }
            if (order.deliveryAddress.city) {
                data['city'] = order.deliveryAddress.city;
            }
            if (order.deliveryAddress.department) {
                data['department'] = order.deliveryAddress.department;
            }
        }
        return this.request('/orders/', { order: data, basket: basketResult }, __WEBPACK_IMPORTED_MODULE_1__angular_http__["e" /* RequestMethod */].Post);
    };
    BackendService.prototype.request = function (url, data, method, handleError, params) {
        if (handleError === void 0) { handleError = true; }
        var requestOptionsArgs = {
            url: __WEBPACK_IMPORTED_MODULE_3__environments_environments__["a" /* environment */].api + url,
            method: method,
            headers: this.headers,
            body: data,
            params: params,
        };
        var requestOptions = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["f" /* RequestOptions */](requestOptionsArgs);
        return this.http.request(new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Request */](requestOptions))
            .timeout(this.timeoutInMillisSec)
            .catch(function (error) {
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Rx__["Observable"].throw(error);
        }).map(function (response) { return response.json(); });
    };
    BackendService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */]])
    ], BackendService);
    return BackendService;
}());

//# sourceMappingURL=backend.service.js.map

/***/ }),

/***/ 355:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ProductModel; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductDetailModel; });
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var ProductModel = /** @class */ (function () {
    function ProductModel() {
        this.title = "";
        this.slug = "";
        this.image_url = "";
        this.short_description = "";
    }
    return ProductModel;
}());

var ProductDetailModel = /** @class */ (function (_super) {
    __extends(ProductDetailModel, _super);
    function ProductDetailModel() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ProductDetailModel;
}(ProductModel));

//# sourceMappingURL=product.model.js.map

/***/ }),

/***/ 356:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuItems; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return MenuItemsForOrder; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__shared_catalog_category_category_model__ = __webpack_require__(140);

var MenuItems = [
    {
        name: "О нас",
        color: "yellow",
        page: "about",
        category: null,
        icon: "about"
    },
    {
        name: "Букеты",
        color: __WEBPACK_IMPORTED_MODULE_0__shared_catalog_category_category_model__["a" /* CategoryThemes */].flowers.color,
        page: "catalog",
        category: "flowers",
        icon: "flowers"
    },
    {
        name: "Шарики",
        color: __WEBPACK_IMPORTED_MODULE_0__shared_catalog_category_category_model__["a" /* CategoryThemes */].balloons.color,
        page: "catalog",
        category: "balloons",
        icon: "balloons"
    },
    {
        name: "Оформление",
        color: __WEBPACK_IMPORTED_MODULE_0__shared_catalog_category_category_model__["a" /* CategoryThemes */].decor.color,
        page: "catalog",
        category: "decor",
        icon: "decor"
    },
    {
        name: "Подарки",
        color: __WEBPACK_IMPORTED_MODULE_0__shared_catalog_category_category_model__["a" /* CategoryThemes */].gifts.color,
        page: "catalog",
        category: "gifts",
        icon: "presents"
    },
    {
        name: "Доставка",
        color: "blue",
        page: "delivery",
        category: null,
        icon: "delivery"
    },
    {
        name: "Оплата",
        color: "yellow",
        page: "payment",
        category: null,
        icon: "payment"
    },
    {
        name: "Контакты",
        color: "blue",
        page: "contacts",
        category: null,
        icon: "contacts"
    }
];
var MenuItemsForOrder = [
    {
        name: "Букеты",
        color: __WEBPACK_IMPORTED_MODULE_0__shared_catalog_category_category_model__["a" /* CategoryThemes */].flowers.color,
        page: "catalog",
        category: "flowers",
        icon: "flowers"
    },
    {
        name: "Шарики",
        color: __WEBPACK_IMPORTED_MODULE_0__shared_catalog_category_category_model__["a" /* CategoryThemes */].balloons.color,
        page: "catalog",
        category: "balloons",
        icon: "balloons"
    },
    {
        name: "Оформление",
        color: __WEBPACK_IMPORTED_MODULE_0__shared_catalog_category_category_model__["a" /* CategoryThemes */].decor.color,
        page: "catalog",
        category: "decor",
        icon: "decor"
    },
    {
        name: "Подарки",
        color: __WEBPACK_IMPORTED_MODULE_0__shared_catalog_category_category_model__["a" /* CategoryThemes */].gifts.color,
        page: "catalog",
        category: "gifts",
        icon: "presents"
    }
];
//# sourceMappingURL=menu.data.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketModalComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__basket_service__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nav_nav_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__user_user_service__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__backend_backend_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_order_order_model__ = __webpack_require__(156);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var BasketModalComponent = /** @class */ (function () {
    function BasketModalComponent(params, backend, basket, viewCtrl, userService, nav) {
        var _this = this;
        this.params = params;
        this.backend = backend;
        this.basket = basket;
        this.viewCtrl = viewCtrl;
        this.userService = userService;
        this.nav = nav;
        this.products = [];
        this.sum = 0;
        this.isSend = false;
        this.user = new __WEBPACK_IMPORTED_MODULE_6__pages_order_order_model__["a" /* OrderModel */]();
        this.basket.getProducts().subscribe(function (products) {
            _this.products = products;
            _this.sum = _this.basket.getSum(_this.products);
        });
        this.basketProducts = this.basket.get();
        this.backend.getDeliveries().subscribe(function (resp) {
            var deliveries = JSON.parse(resp["result"]);
            _this.delivery = deliveries.filter(function (item) { return item.id === _this.params.get('delivery'); })[0];
        });
        this.backend.getPayments().subscribe(function (resp) {
            var payments = JSON.parse(resp["result"]);
            _this.payment = payments.filter(function (item) { return item.id === _this.params.get('payment'); })[0];
        });
    }
    BasketModalComponent.prototype.close = function () {
        this.viewCtrl.dismiss(true);
    };
    BasketModalComponent.prototype.onSubmit = function () {
        this.basket.clear();
        this.saveUser();
    };
    BasketModalComponent.prototype.saveUser = function () {
        var _this = this;
        this.user.fio = this.params.get('fio');
        this.user.email = this.params.get('email');
        this.user.phone = this.params.get('phone');
        this.user.delivery = this.delivery;
        this.user.payment = this.payment;
        this.user.deliveryAddress.city = this.params.get('city');
        this.user.deliveryAddress.address = this.params.get('address');
        this.user.deliveryAddress.department = this.params.get('department');
        this.backend.sendOrder(this.user, this.basketProducts, this.sum).subscribe(function (resp) {
            _this.userService.save(_this.user);
            _this.isSend = true;
        });
    };
    BasketModalComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'basket-modal',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/basket/basket-modal/basket-modal.component.html"*/'<ion-content no-bounce>\n	<div class="basket-modal" *ngIf="!isSend">\n		<h3>Ваш заказ:</h3>\n\n		<div class="basket-modal__goods">\n			<div class="basket-modal__goods-item" *ngFor="let product of products">\n				<div class="basket-modal__goods-image" [style.background-image]="\'url(\' + product.image_url + \')\'"></div>\n				<div class="basket-modal__goods-info">\n					<div class="basket-modal__goods-name">{{product.title}}</div>\n					<div class="basket-modal__goods-count">{{basketProducts[product.id]}} x </div>\n					<div class="basket-modal__goods-price">\n						{{basketProducts[product.id]*product.price | currency}}\n					</div>\n				</div>\n			</div>\n		</div>\n		<h4 class="basket-modal__goods-sum">Итого к оплате: {{sum | currency}}</h4>\n\n		<div class="basket-modal__user-info">\n			<div class="basket-modal__user-info-item">ФИО: {{params.get(\'fio\')}}</div>\n			<div class="basket-modal__user-info-item">Телефон: {{params.get(\'phone\')}}</div>\n			<div class="basket-modal__user-info-item">Email: {{params.get(\'email\')}}</div>\n		</div>\n\n		<div class="basket-modal__user-info-radio" *ngIf="delivery">\n			<div class="basket-modal__user-info-radio-image">\n				<img [src]="delivery.image_url">\n			</div>\n			<div class="basket-modal__user-info-radio-text">\n				<h4>Доставка: {{delivery.name}}</h4>\n				<div class="basket-modal__user-info">\n					<div class="basket-modal__user-info-item" *ngIf="params.get(\'city\')">\n						Город: {{params.get(\'city\')}}\n					</div>\n					<div class="basket-modal__user-info-item" *ngIf="params.get(\'department\')">\n						№ отделения: {{params.get(\'department\')}}\n					</div>\n					<div class="basket-modal__user-info-item" *ngIf="params.get(\'address\')">\n						Адресс: {{params.get(\'address\')}}\n					</div>\n				</div>\n			</div>\n		</div>\n		<div class="basket-modal__user-info-radio" *ngIf="payment">\n			<div class="basket-modal__user-info-radio-image">\n				<img [src]="payment.image_url">\n			</div>\n			<div class="basket-modal__user-info-radio-text">\n				<h4>Оплата: {{payment.name}}</h4>\n			</div>\n		</div>\n\n		<button ion-button color="primary" round class="blue" (click)="onSubmit()" [disabled]="sum === 0">Подтвердить</button>\n	</div>\n\n	<div class="basket-modal basket-modal--success" *ngIf="isSend">\n		<h2>Спасибо за Ваш заказ!</h2>\n		<img src="assets/images/icons/about.svg">\n		<h3>Наш менеджер скоро свяжеться с Вами для уточнения деталей</h3>\n		<button ion-button color="primary" round class="yellow" (click)="close()">Закрыть</button>\n	</div>\n</ion-content>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/basket/basket-modal/basket-modal.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__backend_backend_service__["a" /* BackendService */],
            __WEBPACK_IMPORTED_MODULE_1__basket_service__["a" /* BasketService */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_4__user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_2__nav_nav_service__["a" /* NavService */]])
    ], BasketModalComponent);
    return BasketModalComponent;
}());

//# sourceMappingURL=basket-modal.component.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(364);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(401);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_main_main__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_catalog_catalog__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_catalog_catalog_categories_catalog_categories_component__ = __webpack_require__(690);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_catalog_catalog_item_catalog_item_component__ = __webpack_require__(691);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_catalog_catalog_detail_catalog_detail_component__ = __webpack_require__(692);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__shared_menu_menu_component__ = __webpack_require__(693);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__shared_nav_nav_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__shared_nav_nav_header_nav_header_component__ = __webpack_require__(694);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__shared_nav_nav_footer_nav_footer_component__ = __webpack_require__(695);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__shared_nav_nav_header_nav_header_service__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_about_about_page__ = __webpack_require__(696);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact_page__ = __webpack_require__(697);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_delivery_delivery_page__ = __webpack_require__(698);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_payment_payment_page__ = __webpack_require__(699);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__shared_animate_animate_aside_animate_aside_directive__ = __webpack_require__(700);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__shared_animate_animate_image_animate_image_directive__ = __webpack_require__(701);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__shared_basket_basket_directive__ = __webpack_require__(702);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__shared_basket_basket_badge_basket_badge_component__ = __webpack_require__(703);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__shared_basket_basket_order_basket_order_component__ = __webpack_require__(704);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__shared_basket_basket_service__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__shared_basket_basket_modal_basket_modal_component__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__shared_price_price_pipe__ = __webpack_require__(705);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_order_order_page__ = __webpack_require__(706);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__shared_user_user_service__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__shared_backend_backend_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__angular_platform_browser_animations__ = __webpack_require__(707);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33_ionic_tooltips__ = __webpack_require__(709);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_main_main__["a" /* MainPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_catalog_catalog__["a" /* CatalogPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_order_order_page__["a" /* OrderPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_catalog_catalog_categories_catalog_categories_component__["a" /* CatalogCategoriesComponent */],
                __WEBPACK_IMPORTED_MODULE_8__pages_catalog_catalog_item_catalog_item_component__["a" /* CatalogItemComponent */],
                __WEBPACK_IMPORTED_MODULE_9__pages_catalog_catalog_detail_catalog_detail_component__["a" /* CatalogDetailComponent */],
                __WEBPACK_IMPORTED_MODULE_10__shared_menu_menu_component__["a" /* MenuComponent */],
                __WEBPACK_IMPORTED_MODULE_14__shared_nav_nav_header_nav_header_component__["a" /* NavHeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_15__shared_nav_nav_footer_nav_footer_component__["a" /* NavFooterComponent */],
                __WEBPACK_IMPORTED_MODULE_24__shared_basket_basket_badge_basket_badge_component__["a" /* BasketBadgeComponent */],
                __WEBPACK_IMPORTED_MODULE_25__shared_basket_basket_order_basket_order_component__["a" /* BasketOrderComponent */],
                __WEBPACK_IMPORTED_MODULE_27__shared_basket_basket_modal_basket_modal_component__["a" /* BasketModalComponent */],
                __WEBPACK_IMPORTED_MODULE_17__pages_about_about_page__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact_page__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_delivery_delivery_page__["a" /* DeliveryPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_payment_payment_page__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_21__shared_animate_animate_aside_animate_aside_directive__["a" /* AnimateAsideDirective */],
                __WEBPACK_IMPORTED_MODULE_22__shared_animate_animate_image_animate_image_directive__["a" /* AnimateImageDirective */],
                __WEBPACK_IMPORTED_MODULE_23__shared_basket_basket_directive__["a" /* BasketDirective */],
                __WEBPACK_IMPORTED_MODULE_28__shared_price_price_pipe__["a" /* PricePipe */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_32__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_33_ionic_tooltips__["a" /* TooltipsModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { component: __WEBPACK_IMPORTED_MODULE_17__pages_about_about_page__["a" /* AboutPage */], name: 'about', segment: 'about' },
                        { component: __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact_page__["a" /* ContactPage */], name: 'contacts', segment: 'contacts' },
                        { component: __WEBPACK_IMPORTED_MODULE_19__pages_delivery_delivery_page__["a" /* DeliveryPage */], name: 'delivery', segment: 'delivery' },
                        { component: __WEBPACK_IMPORTED_MODULE_20__pages_payment_payment_page__["a" /* PaymentPage */], name: 'payment', segment: 'payment' },
                        { component: __WEBPACK_IMPORTED_MODULE_5__pages_main_main__["a" /* MainPage */], name: 'main', segment: 'main' },
                        { component: __WEBPACK_IMPORTED_MODULE_6__pages_catalog_catalog__["a" /* CatalogPage */], name: 'catalog', segment: 'catalog/:category/:subcategory' },
                        { component: __WEBPACK_IMPORTED_MODULE_9__pages_catalog_catalog_detail_catalog_detail_component__["a" /* CatalogDetailComponent */], name: 'product', segment: 'product/:slug' },
                        { component: __WEBPACK_IMPORTED_MODULE_29__pages_order_order_page__["a" /* OrderPage */], name: 'order', segment: 'order' }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_main_main__["a" /* MainPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_catalog_catalog__["a" /* CatalogPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_order_order_page__["a" /* OrderPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_catalog_catalog_categories_catalog_categories_component__["a" /* CatalogCategoriesComponent */],
                __WEBPACK_IMPORTED_MODULE_8__pages_catalog_catalog_item_catalog_item_component__["a" /* CatalogItemComponent */],
                __WEBPACK_IMPORTED_MODULE_9__pages_catalog_catalog_detail_catalog_detail_component__["a" /* CatalogDetailComponent */],
                __WEBPACK_IMPORTED_MODULE_10__shared_menu_menu_component__["a" /* MenuComponent */],
                __WEBPACK_IMPORTED_MODULE_14__shared_nav_nav_header_nav_header_component__["a" /* NavHeaderComponent */],
                __WEBPACK_IMPORTED_MODULE_15__shared_nav_nav_footer_nav_footer_component__["a" /* NavFooterComponent */],
                __WEBPACK_IMPORTED_MODULE_17__pages_about_about_page__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_contact_contact_page__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_delivery_delivery_page__["a" /* DeliveryPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_payment_payment_page__["a" /* PaymentPage */],
                __WEBPACK_IMPORTED_MODULE_24__shared_basket_basket_badge_basket_badge_component__["a" /* BasketBadgeComponent */],
                __WEBPACK_IMPORTED_MODULE_27__shared_basket_basket_modal_basket_modal_component__["a" /* BasketModalComponent */],
                __WEBPACK_IMPORTED_MODULE_25__shared_basket_basket_order_basket_order_component__["a" /* BasketOrderComponent */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_11__shared_nav_nav_service__["a" /* NavService */],
                __WEBPACK_IMPORTED_MODULE_16__shared_nav_nav_header_nav_header_service__["a" /* NavHeaderService */],
                __WEBPACK_IMPORTED_MODULE_30__shared_user_user_service__["a" /* UserService */],
                __WEBPACK_IMPORTED_MODULE_28__shared_price_price_pipe__["a" /* PricePipe */],
                __WEBPACK_IMPORTED_MODULE_26__shared_basket_basket_service__["a" /* BasketService */],
                __WEBPACK_IMPORTED_MODULE_31__shared_backend_backend_service__["a" /* BackendService */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 401:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(256);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(261);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_main_main__ = __webpack_require__(262);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_nav_nav_service__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, navService) {
        var _this = this;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_main_main__["a" /* MainPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            navService.ready(_this.nav);
        });
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('content'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/app/app.html"*/'<aside-menu></aside-menu>\n<ion-nav [root]="rootPage" main #content></ion-nav>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_5__shared_nav_nav_service__["a" /* NavService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CatalogPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_catalog_category_category_model__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_nav_nav_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_event_event_enum__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_backend_backend_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_animejs__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_animejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_animejs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var CatalogPage = /** @class */ (function () {
    function CatalogPage(navParams, nav, events, backend, elementRef) {
        var _this = this;
        this.navParams = navParams;
        this.nav = nav;
        this.events = events;
        this.backend = backend;
        this.elementRef = elementRef;
        this.limit = 4;
        this.page = 1;
        this.totalCount = 0;
        this.products = [];
        this.isOpenCategory = false;
        this.categories = [];
        this.title = "";
        this.animate = null;
        console.log('this.navParams', this.navParams);
        this.category = this.navParams.get('category');
        this.subcategory = this.navParams.get('subcategory');
        this.currentTheme = __WEBPACK_IMPORTED_MODULE_2__shared_catalog_category_category_model__["b" /* CurrentTheme */].getTheme(this.category);
        this.resizeSlider();
        this.events.subscribe(__WEBPACK_IMPORTED_MODULE_4__shared_event_event_enum__["a" /* EventTopic */].OpenProduct, function (slug) {
            _this.openProduct(slug);
        });
        this.backend.getCategory(this.category).subscribe(function (category) {
            _this.categories = category["subcategories"];
            _this.title = category["title"];
        });
        this.backend.getProducts(this.category, this.subcategory, this.limit, this.page).subscribe(function (result) {
            _this.products = JSON.parse(result['products']);
            _this.totalCount = result['count'];
        });
    }
    CatalogPage.prototype.ngOnDestroy = function () {
        this.events.unsubscribe(__WEBPACK_IMPORTED_MODULE_4__shared_event_event_enum__["a" /* EventTopic */].OpenProduct);
    };
    CatalogPage.prototype.ionViewDidEnter = function () {
        if (this.animate) {
            this.animate.reverse();
            this.animate.play();
        }
    };
    CatalogPage.prototype.resizeSlider = function () {
        var width = window.innerWidth;
        if (width <= 700) {
            this.limit = 1;
            return;
        }
        if (width <= 800) {
            this.limit = 2;
            return;
        }
        if (width <= 1920) {
            this.limit = 3;
            return;
        }
    };
    CatalogPage.prototype.openProduct = function (slug) {
        var el = this.elementRef.nativeElement;
        this.animate = __WEBPACK_IMPORTED_MODULE_6_animejs__["timeline"]();
        this.animate.add(this.animateTitle(el))
            .add(this.animateCategory(el))
            .add(this.animateCatalog(el, slug));
    };
    CatalogPage.prototype.toogleCategory = function () {
        this.isOpenCategory = !this.isOpenCategory;
    };
    CatalogPage.prototype.changeCategory = function (subcategory) {
        this.nav.goTo('catalog', { category: this.category, subcategory: subcategory.slug });
    };
    CatalogPage.prototype.next = function () {
        this.slides.slideNext();
    };
    CatalogPage.prototype.prev = function () {
        this.slides.slidePrev();
    };
    CatalogPage.prototype.onSlideDidChange = function (event) {
        var _this = this;
        if (event.isEnd() && this.showLoader()) {
            this.page++;
            this.backend.getProducts(this.category, this.subcategory, this.limit, this.page).subscribe(function (result) {
                _this.products = _this.products.concat(JSON.parse(result['products']));
            });
        }
    };
    CatalogPage.prototype.showLoader = function () {
        return !(this.products.length == this.totalCount);
    };
    CatalogPage.prototype.animateTitle = function (el) {
        return {
            targets: [el.querySelector('h1')],
            duration: 1000,
            opacity: 0,
            translateY: '-100%',
        };
    };
    CatalogPage.prototype.animateCategory = function (el) {
        return {
            targets: [el.querySelector('h1')],
            duration: 1000,
            opacity: 0,
            translateX: '100%',
        };
    };
    CatalogPage.prototype.animateCatalog = function (el, slug) {
        var _this = this;
        return {
            targets: [
                el.querySelector('.catalog'),
                el.querySelector('.general-img')
            ],
            duration: 1000,
            opacity: 0,
            translateY: '100%',
            delay: function (elem, i, l) {
                return (i * 200);
            },
            complete: function (el) {
                if (slug !== '')
                    _this.nav.goTo('product', { slug: slug });
            }
        };
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Slides */])
    ], CatalogPage.prototype, "slides", void 0);
    CatalogPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'catalog',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/catalog/catalog.html"*/'<ion-content class="catalog-page" [ngClass]="currentTheme.color" no-bounce [class.open-category]="isOpenCategory">\n	<nav-header></nav-header>\n	<h1 (click)="toogleCategory()">{{title}}<div class="category-open"><img src="assets/images/icons/arrow-white.svg"></div></h1>\n	<div class="catalog">\n		<div class="catalog-inner" [class.open-category]="isOpenCategory">\n			<ion-slides [slidesPerView]="limit" pager="true" (ionSlideDidChange)="onSlideDidChange($event)">\n			  <ion-slide *ngFor="let product of products">\n			    <catalog-item [product]="product" [color]="currentTheme.color"></catalog-item>\n			  </ion-slide>\n			  <ion-slide *ngIf="showLoader()">\n			  	<div class="catalog-page__loader">\n				  	<ion-spinner name="crescent"></ion-spinner>\n				  	Загружаем еще товары...\n				</div>\n			  </ion-slide>\n			</ion-slides>\n			<div class="arrows-wrap">\n				<div class="arrows">\n					<div class="arrow-prev" (click)="prev()" *ngIf="!slides.isBeginning()">\n						<img src="assets/images/icons/arrow-{{currentTheme.color}}.svg">\n					</div>\n					<div class="arrow-next" (click)="next()" *ngIf="!slides.isEnd()">\n						<img src="assets/images/icons/arrow-{{currentTheme.color}}.svg">\n					</div>\n				</div>\n			</div>\n		</div>\n		<catalog-categories [categories]="categories" [color]="currentTheme.color" [isOpen]="isOpenCategory" (openCategory)="changeCategory($event)"></catalog-categories>\n	</div>\n\n	\n	<img class="general-img" *ngIf="currentTheme.theme===\'balloons\'" src="assets/images/bg/balloon5.svg">\n	<!--<img class="balloon-1 blur" *ngIf="currentTheme.theme===\'balloons\'" src="assets/images/bg/balloon2.svg">\n	<img class="menu-image" *ngIf="currentTheme.theme===\'balloons\'" src="assets/images/bg/balloon2.svg">\n	<img class="balloon-3 blur" *ngIf="currentTheme.theme===\'balloons\'" src="assets/images/bg/balloon3.svg">-->\n\n\n\n	<img class="general-img" *ngIf="currentTheme.theme===\'flowers\'" src="assets/images/bg/flowers03.svg">\n	<!--<img class="menu-image menu-image__left" *ngIf="currentTheme.theme===\'flowers\'" src="assets/images/bg/flowers01.svg">\n	<img class="balloon-1" *ngIf="currentTheme.theme===\'flowers\'" src="assets/images/bg/flowers02.svg">\n	-->\n	<img class="general-img menu-image__left" *ngIf="currentTheme.theme===\'gifts\'" src="assets/images/bg/gift1.svg">\n	<!--<img class="balloon-1 blur" *ngIf="currentTheme.theme===\'gifts\'" src="assets/images/bg/gift2.svg">\n	<img class="balloon-3 blur" *ngIf="currentTheme.theme===\'gifts\'" src="assets/images/bg/gift3.svg">\n	<img class="menu-image menu-image__left" *ngIf="currentTheme.theme===\'gifts\'" src="assets/images/bg/gift4.svg">-->\n	<nav-footer></nav-footer>\n</ion-content>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/catalog/catalog.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__shared_nav_nav_service__["a" /* NavService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */],
            __WEBPACK_IMPORTED_MODULE_5__shared_backend_backend_service__["a" /* BackendService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], CatalogPage);
    return CatalogPage;
}());

//# sourceMappingURL=catalog.js.map

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    api: "https://api.pozitiff.in.ua",
    address: {
        title: "Ирпень, ул. Славянская, 35",
        link: "https://www.google.com/maps/place/%D0%93%D1%83%D0%BB%D0%B8%D0%B2%D0%B5%D1%80/@50.5204642,30.2403301,16.97z/data=!4m13!1m7!3m6!1s0x472b33d30f42e483:0x398df907db8e4e6a!2z0YPQuy4g0KHQu9Cw0LLRj9C90YHQutCw0Y8sIDM1LCDQmNGA0L_QtdC90YwsINCa0LjQtdCy0YHQutCw0Y8g0L7QsdC70LDRgdGC0YwsIDA4MjAw!3b1!8m2!3d50.5204981!4d30.2423805!3m4!1s0x472b33d30f10640b:0xcbe8005ffa3b63b6!8m2!3d50.5205802!4d30.2422929"
    },
    email: "hello@pozitiff.in.ua",
    phones: [
        {
            title: "+38 066 45 38 367",
            link: "tel:+380664538367"
        },
        {
            title: "+38 067 64 71 467",
            link: "tel:+380676471467"
        }
    ],
    socials: {
        fb: "https://www.facebook.com/groups/446618002569718/",
        instagram: "https://www.instagram.com/prazdnik.pozitiv.irpen/"
    },
    reference: "https://mrgnc.io/"
};
//# sourceMappingURL=environments.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_backend_backend_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_event_event_enum__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__ = __webpack_require__(263);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var BasketService = /** @class */ (function () {
    function BasketService(toastCtrl, backend, events) {
        var _this = this;
        this.toastCtrl = toastCtrl;
        this.backend = backend;
        this.events = events;
        this.products = [];
        this.getProducts().subscribe(function (products) {
            _this.products = products;
        });
    }
    BasketService.prototype.getProducts = function () {
        var _this = this;
        return new __WEBPACK_IMPORTED_MODULE_4_rxjs_Rx__["Observable"](function (observer) {
            var basket = _this.get();
            var ids = Object.keys(basket);
            _this.backend.getProductsByIds(ids).subscribe(function (resp) {
                observer.next(JSON.parse(resp["products"]));
            });
        });
    };
    BasketService.prototype.add = function (id) {
        var items = this.get();
        if (items.hasOwnProperty(id)) {
            items[id] = items[id] + 1;
        }
        else {
            items[id] = 1;
        }
        this.set(items);
        this.presentToast('Товар успешно добавлен в корзину!');
    };
    BasketService.prototype.getCount = function () {
        var items = this.get();
        var count = 0;
        for (var i in items) {
            count += items[i];
        }
        return count;
    };
    BasketService.prototype.get = function () {
        return JSON.parse(localStorage.getItem('basket')) || {};
    };
    BasketService.prototype.set = function (items) {
        localStorage.setItem('basket', JSON.stringify(items));
        ;
    };
    BasketService.prototype.getSum = function (products) {
        var sum = 0;
        var basket = this.get();
        if (basket.length === 0) {
            products = [];
            return sum;
        }
        products.map(function (product) {
            sum += product.price * basket[product.id];
        });
        return sum;
    };
    BasketService.prototype.clear = function () {
        this.set({});
        this.events.publish(__WEBPACK_IMPORTED_MODULE_3__shared_event_event_enum__["a" /* EventTopic */].BasketCleaned);
    };
    BasketService.prototype.enableDelivery = function () {
        var isEnable = true;
        this.products.map(function (product) {
            if (product.delivery === false) {
                isEnable = false;
                return;
            }
        });
        return isEnable;
    };
    BasketService.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    BasketService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */], __WEBPACK_IMPORTED_MODULE_2__shared_backend_backend_service__["a" /* BackendService */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* Events */]])
    ], BasketService);
    return BasketService;
}());

//# sourceMappingURL=basket.service.js.map

/***/ }),

/***/ 690:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CatalogCategoriesComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_nav_nav_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CatalogCategoriesComponent = /** @class */ (function () {
    function CatalogCategoriesComponent(nav, navParams) {
        this.nav = nav;
        this.navParams = navParams;
        this.color = 'yellow';
        this.categories = [];
        this.isOpen = false;
        this.openCategory = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
    }
    CatalogCategoriesComponent.prototype.goTo = function (subcategory) {
        this.openCategory.emit(subcategory);
    };
    CatalogCategoriesComponent.prototype.next = function () {
        this.slides.slideNext();
    };
    CatalogCategoriesComponent.prototype.prev = function () {
        this.slides.slidePrev();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Slides */])
    ], CatalogCategoriesComponent.prototype, "slides", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('color'),
        __metadata("design:type", String)
    ], CatalogCategoriesComponent.prototype, "color", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('categories'),
        __metadata("design:type", Array)
    ], CatalogCategoriesComponent.prototype, "categories", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('isOpen'),
        __metadata("design:type", Boolean)
    ], CatalogCategoriesComponent.prototype, "isOpen", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])('openCategory'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], CatalogCategoriesComponent.prototype, "openCategory", void 0);
    CatalogCategoriesComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'catalog-categories',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/catalog/catalog-categories/catalog-categories.component.html"*/'<div class="catalog-categories" [class.open]="isOpen">\n	<ion-slides slidesPerView="6" direction="vertical">\n	  <ion-slide *ngFor="let item of categories">\n	    <div class="catalog-categories__title" (click)=goTo(item)>{{item.title}}</div>\n	  </ion-slide>\n	</ion-slides>\n	<div class="arrow-up" (click)="prev()" *ngIf="!slides.isBeginning()">\n		<img src="assets/images/icons/arrow-{{color}}.svg">\n	</div>\n	<div class="arrow-down" (click)="next()" *ngIf="!slides.isEnd()">\n		<img src="assets/images/icons/arrow-{{color}}.svg">\n	</div>\n</div>\n'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/catalog/catalog-categories/catalog-categories.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_nav_nav_service__["a" /* NavService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* NavParams */]])
    ], CatalogCategoriesComponent);
    return CatalogCategoriesComponent;
}());

//# sourceMappingURL=catalog-categories.component.js.map

/***/ }),

/***/ 691:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CatalogItemComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_catalog_product_product_model__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_event_event_enum__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(20);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CatalogItemComponent = /** @class */ (function () {
    function CatalogItemComponent(events) {
        this.events = events;
        this.color = 'yellow';
    }
    CatalogItemComponent.prototype.openProduct = function (slug) {
        this.events.publish(__WEBPACK_IMPORTED_MODULE_2__shared_event_event_enum__["a" /* EventTopic */].OpenProduct, slug);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('color'),
        __metadata("design:type", String)
    ], CatalogItemComponent.prototype, "color", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('product'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__shared_catalog_product_product_model__["b" /* ProductModel */])
    ], CatalogItemComponent.prototype, "product", void 0);
    CatalogItemComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'catalog-item',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/catalog/catalog-item/catalog-item.component.html"*/'<div class="catalog-item">\n	<div class="catalog-item__image" (click)="openProduct(product.slug)" [class.catalog-item__image--small]="product.buy && product.price > 0" [style.background-image]="\'url(\' + product.image_url + \')\'">\n		<div *ngIf="product.delivery" class="catalog-item__delivery" tooltip="Возможна оставка Новой Почтой!" positionV="bottom" ></div>\n	</div>\n	<div class="catalog-item__info">\n		<div class="catalog-item__title" (click)="openProduct(product.slug)" [ngClass]="color">{{product.title}}</div>\n		<div class="catalog-item__description" [innerHTML]="product.short_description"></div>\n		<div class="catalog-item__price-info" *ngIf="product.buy && product.price > 0">\n			<div class="catalog-item__price">{{product.price | currency}}</div>\n			<button ion-button color="primary" round class="catalog-item__button" [add-to-basket]="product.id" [ngClass]="color">Заказать</button>\n		</div>\n	</div>\n</div>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/catalog/catalog-item/catalog-item.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* Events */]])
    ], CatalogItemComponent);
    return CatalogItemComponent;
}());

//# sourceMappingURL=catalog-item.component.js.map

/***/ }),

/***/ 692:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CatalogDetailComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_catalog_category_category_model__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_catalog_product_product_model__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__shared_nav_nav_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_backend_backend_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CatalogDetailComponent = /** @class */ (function () {
    function CatalogDetailComponent(navParams, nav, backend) {
        var _this = this;
        this.navParams = navParams;
        this.nav = nav;
        this.backend = backend;
        this.isProductLoaded = false;
        this.product = new __WEBPACK_IMPORTED_MODULE_3__shared_catalog_product_product_model__["a" /* ProductDetailModel */]();
        this.backend.getProduct(this.navParams.get('slug')).subscribe(function (result) {
            var product = JSON.parse(result["product"]);
            _this.currentTheme = __WEBPACK_IMPORTED_MODULE_2__shared_catalog_category_category_model__["b" /* CurrentTheme */].getTheme(product.category.slug);
            _this.product = product;
            _this.product.images = [];
            _this.product.images.push({ url: _this.product.image_url });
            _this.product.images = _this.product.images.concat(result["images"]);
            _this.isProductLoaded = true;
        });
    }
    CatalogDetailComponent.prototype.openProduct = function (slug) {
        this.nav.goTo('product', { slug: slug });
    };
    CatalogDetailComponent.prototype.back = function () {
        this.nav.goTo('catalog', { category: this.product.category.slug, subcategory: "all" });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Slides */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Slides */])
    ], CatalogDetailComponent.prototype, "slides", void 0);
    CatalogDetailComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'catalog-detail',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/catalog/catalog-detail/catalog-detail.component.html"*/'<ion-content class="static-page catalog-detail" *ngIf="isProductLoaded" [ngClass]="currentTheme.color" no-bounce>\n	<nav-header></nav-header>\n\n	<div class="static-page__wrap">\n		<div class="static-page__left" animate-image>\n			<div *ngIf="product.delivery" class="catalog-detail__delivery" tooltip="Возможна оставка Новой Почтой!" positionV="bottom" ></div>\n			<ion-slides [pager]="product.images.length > 1" spaceBetween="30">\n			  <ion-slide *ngFor="let image of product.images">\n			    <div class="static-page__image" [style.background-image]="\'url(\' + image.url + \')\'"></div>\n			  </ion-slide>\n			</ion-slides>\n		</div>\n		<div class="static-page__right" animate-aside>\n			<div class="catalog-detail__category" (click)="back()">{{ product.subcategory ? product.subcategory.title : product.category.title}}</div>\n			<h1 class="static-page__title" [ngClass]="currentTheme.color">\n				{{product.title}}\n			</h1>\n			<div class="static-page__description" [innerHTML]="product.description">\n			</div>\n			<div class="catalog-detail__buy"  *ngIf="product.buy && product.price > 0">\n				<button ion-button color="primary" round class="catalog-detail__button" [ngClass]="currentTheme.color" [add-to-basket]="product.id">Заказать</button>\n				<div class="catalog-detail__price">{{product.price | currency}}</div>\n			</div>\n			<div class="catalog-detail__arrows-wrap">\n				<div class="catalog-detail__arrows">\n					<div class="catalog-detail__arrow-prev" *ngIf="product.prev" (click)="openProduct(product.prev.slug)" tooltip="Открыть предыдущий товар из этой категории" positionV="bottom" >\n						<img src="assets/images/icons/arrow-{{currentTheme.color}}.svg">\n					</div>\n					<div class="catalog-detail__arrow-next" *ngIf="product.next" (click)="openProduct(product.next.slug)" tooltip="Открыть следующий товар из этой категории" positionV="bottom" >\n						<img src="assets/images/icons/arrow-{{currentTheme.color}}.svg">\n					</div>\n				</div>\n			</div>\n			<img class="static-page__illustration" *ngIf="currentTheme.theme===\'balloons\'" src="assets/images/bg/balloon7.svg">\n			<nav-footer></nav-footer>\n		</div>\n	</div>\n</ion-content>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/catalog/catalog-detail/catalog-detail.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__shared_nav_nav_service__["a" /* NavService */],
            __WEBPACK_IMPORTED_MODULE_5__shared_backend_backend_service__["a" /* BackendService */]])
    ], CatalogDetailComponent);
    return CatalogDetailComponent;
}());

//# sourceMappingURL=catalog-detail.component.js.map

/***/ }),

/***/ 693:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MenuComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_nav_nav_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__menu_data__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__shared_nav_nav_header_nav_header_service__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environments__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var MenuComponent = /** @class */ (function () {
    function MenuComponent(nav, navHeader) {
        this.nav = nav;
        this.navHeader = navHeader;
        this.menuItems = __WEBPACK_IMPORTED_MODULE_2__menu_data__["a" /* MenuItems */];
        this.env = __WEBPACK_IMPORTED_MODULE_4__environments_environments__["a" /* environment */];
    }
    MenuComponent.prototype.toogleMenu = function () {
        this.navHeader.toogleMenu();
    };
    MenuComponent.prototype.goTo = function (page, category) {
        this.navHeader.toogleMenu();
        this.nav.goTo(page, { category: category, subcategory: 'all' });
    };
    MenuComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'aside-menu',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/menu/menu.component.html"*/'<div class="menu" [class.menu--open]="navHeader.menuOpen">\n	<nav-header></nav-header>\n	<div (click)="nav.goHome()" class="menu__logo">\n		<img src="assets/images/menu/logo.png">\n	</div>\n	<div  (click)="nav.goHome()" class="menu__logo--small">\n		<img src="assets/images/menu/logo-small.png">\n	</div>\n	<div class="menu__item-wrap" *ngFor="let item of menuItems" (click)="goTo(item.page, item.category)">\n		<button ion-button color="light" class="menu__item menu__item--{{item.color}}">\n			<div class="menu__icon">\n				<img src="assets/images/icons/{{item.icon}}.svg">\n			</div>\n			<div class="menu__title">{{item.name}}</div>\n		</button>\n		<div class="menu__tooltip">{{item.name}}</div>\n	</div>\n	<div class="menu__footer">\n		<a [href]="env.reference" target="_blank">\n			<img src="assets/images/menu/relogo.png">\n		</a>\n		<div>© 2018 Pozitiff. All rights reserved</div>\n	</div>\n</div>\n\n'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/menu/menu.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_nav_nav_service__["a" /* NavService */], __WEBPACK_IMPORTED_MODULE_3__shared_nav_nav_header_nav_header_service__["a" /* NavHeaderService */]])
    ], MenuComponent);
    return MenuComponent;
}());

//# sourceMappingURL=menu.component.js.map

/***/ }),

/***/ 694:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__nav_header_service__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nav_service__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavHeaderComponent = /** @class */ (function () {
    function NavHeaderComponent(navHeader, nav) {
        this.navHeader = navHeader;
        this.nav = nav;
    }
    NavHeaderComponent.prototype.goHome = function () {
        if (this.navHeader.menuOpen === true) {
            this.navHeader.toogleMenu();
        }
        this.nav.goHome();
    };
    NavHeaderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'nav-header',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/nav/nav-header/nav-header.component.html"*/'<div class="mobile-header">\n	<div (click)="goHome()" class="menu__logo--small">\n		<img src="assets/images/menu/logo-small.png">\n	</div>\n\n	<button ion-button color="light" class="menu-button" [class.menu-button--open]="navHeader.menuOpen" (click)="navHeader.toogleMenu()">\n		<span class="menu-block"></span>\n	</button>\n</div>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/nav/nav-header/nav-header.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__nav_header_service__["a" /* NavHeaderService */], __WEBPACK_IMPORTED_MODULE_2__nav_service__["a" /* NavService */]])
    ], NavHeaderComponent);
    return NavHeaderComponent;
}());

//# sourceMappingURL=nav-header.component.js.map

/***/ }),

/***/ 695:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavFooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__nav_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environments__ = __webpack_require__(45);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var NavFooterComponent = /** @class */ (function () {
    function NavFooterComponent(nav) {
        this.nav = nav;
        this.env = __WEBPACK_IMPORTED_MODULE_2__environments_environments__["a" /* environment */];
    }
    NavFooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'nav-footer',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/nav/nav-footer/nav-footer.component.html"*/'<div class="nav-footer">\n	<div class="nav-footer__copyrights">\n		<a [href]="env.reference">\n			<img src="assets/images/menu/relogo.png">\n		</a>\n		<div>© 2018 Pozitiff. All rights reserved</div>\n	</div>\n	<basket-badge></basket-badge>\n</div>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/nav/nav-footer/nav-footer.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__nav_service__["a" /* NavService */]])
    ], NavFooterComponent);
    return NavFooterComponent;
}());

//# sourceMappingURL=nav-footer.component.js.map

/***/ }),

/***/ 696:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_backend_backend_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(backend) {
        var _this = this;
        this.backend = backend;
        this.backend.getPage('about').subscribe(function (resp) {
            _this.page = JSON.parse(resp["result"]);
        });
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/about/about.page.html"*/'<ion-content class="static-page about-page yellow" no-bounce>\n	<nav-header></nav-header>\n	<div class="static-page__wrap" *ngIf="page">\n		<div class="static-page__left" animate-image>\n			<div class="static-page__image" [style.background-image]="\'url(\' + page.image_url + \')\'"></div>\n		</div>\n		<div class="static-page__right" animate-aside>\n			<h1 #title class="static-page__title yellow">{{page.name}}</h1>\n			<div #description class="static-page__description" [innerHTML]="page.description"></div>\n			<nav-footer></nav-footer>\n		</div>\n	</div>\n</ion-content>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/about/about.page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_backend_backend_service__["a" /* BackendService */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.page.js.map

/***/ }),

/***/ 697:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environments__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_backend_backend_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ContactPage = /** @class */ (function () {
    function ContactPage(backend) {
        var _this = this;
        this.backend = backend;
        this.env = __WEBPACK_IMPORTED_MODULE_1__environments_environments__["a" /* environment */];
        this.backend.getPage('contact').subscribe(function (resp) {
            _this.page = JSON.parse(resp["result"]);
        });
    }
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/contact/contact.page.html"*/'<ion-content class="static-page contact-page blue" no-bounce>\n	<nav-header></nav-header>\n	<div class="static-page__wrap" *ngIf="page">\n		<div class="static-page__left"  animate-image>\n			<div class="static-page__image" [style.background-image]="\'url(\' + page.image_url + \')\'">\n			</div>\n		</div>\n		<div class="static-page__right" animate-aside>\n			<h1 #title class="static-page__title blue">{{page.name}}</h1>\n			<div #description class="static-page__description" [innerHTML]="page.description">\n			</div>\n\n			<ul #list class="static-page__list">\n				<li class="static-page__list-item">\n					<div class="static-page__list-item-image">\n						<img src="assets/images/icons/contact1.svg">\n					</div>\n					<div class="static-page__list-item-info">\n						<div class="static-page__list-item-title">Позвони нам</div>\n						<div class="static-page__list-item-description">\n							<a *ngFor="let phone of env.phones" [href]="phone.link">{{phone.title}}<br></a>\n							<a href="mailto:{{env.email}}">{{env.email}}</a>\n						</div>\n					</div>\n				</li>\n				<li class="static-page__list-item">\n					<div class="static-page__list-item-image">\n						<img src="assets/images/icons/delivery3.svg">\n					</div>\n					<div class="static-page__list-item-info">\n						<div class="static-page__list-item-title">Наш адрес</div>\n						<div class="static-page__list-item-description">\n							Мы всегда рады видеть вас в нашем магазине <br>\n							<a [href]="env.address.link" target="_blank">{{env.address.title}}</a>\n						</div>\n					</div>\n				</li>\n				<li class="static-page__list-item">\n					<div class="static-page__list-item-image">\n						<img src="assets/images/icons/share.svg">\n					</div>\n					<div class="static-page__list-item-info">\n						<div class="static-page__list-item-title">Мы в социальных сетях</div>\n						<div class="static-page__list-item-description">\n							Следите за новыми работами и скидками <br>\n							<a [href]="env.socials.fb" class="static-page__list-item-image">\n								<img src="assets/images/icons/fb.svg">\n							</a>\n							<a [href]="env.socials.instagram" class="static-page__list-item-image">\n								<img src="assets/images/icons/instagram.svg">\n							</a>\n						</div>\n					</div>\n				</li>\n			</ul>\n			<nav-footer></nav-footer>\n		</div>\n	</div>\n</ion-content>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/contact/contact.page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__shared_backend_backend_service__["a" /* BackendService */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.page.js.map

/***/ }),

/***/ 698:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DeliveryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__environments_environments__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__shared_backend_backend_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DeliveryPage = /** @class */ (function () {
    function DeliveryPage(backend) {
        var _this = this;
        this.backend = backend;
        this.env = __WEBPACK_IMPORTED_MODULE_1__environments_environments__["a" /* environment */];
        this.deliveries = [];
        this.backend.getDeliveries().subscribe(function (resp) {
            _this.deliveries = JSON.parse(resp["result"]);
        });
        this.backend.getPage('delivery').subscribe(function (resp) {
            _this.page = JSON.parse(resp["result"]);
        });
    }
    DeliveryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-delivery',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/delivery/delivery.page.html"*/'<ion-content class="static-page delivery-page blue" no-bounce>\n	<nav-header></nav-header>\n	<div class="static-page__wrap" *ngIf="page">\n		<div class="static-page__left" animate-image>\n			<div class="static-page__image" [style.background-image]="\'url(\' + page.image_url + \')\'"></div>\n		</div>\n		<div class="static-page__right" animate-aside>\n			<h1 #title class="static-page__title blue">{{page.name}}</h1>\n			<div #description class="static-page__description" [innerHTML]="page.description"></div>\n\n			<ul #list class="static-page__list" *ngIf="deliveries.length > 0">\n				<li class="static-page__list-item" *ngFor="let delivery of deliveries">\n					<div class="static-page__list-item-image">\n						<img [src]="delivery.image_url">\n					</div>\n					<div class="static-page__list-item-info">\n						<div class="static-page__list-item-title">{{delivery.name}}</div>\n						<div class="static-page__list-item-description">\n							{{delivery.description}}\n							<br>\n							<a *ngIf="delivery.link" [href]="env.address.link">{{env.address.title}}</a>\n						</div>\n					</div>\n				</li>\n			</ul>\n\n			<img #illustartion class="static-page__illustration" [src]="page.illustration_url">\n			<nav-footer></nav-footer>\n		</div>\n	</div>\n</ion-content>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/delivery/delivery.page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__shared_backend_backend_service__["a" /* BackendService */]])
    ], DeliveryPage);
    return DeliveryPage;
}());

//# sourceMappingURL=delivery.page.js.map

/***/ }),

/***/ 699:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaymentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__shared_backend_backend_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PaymentPage = /** @class */ (function () {
    function PaymentPage(backend) {
        var _this = this;
        this.backend = backend;
        this.payments = [];
        this.backend.getPayments().subscribe(function (resp) {
            _this.payments = JSON.parse(resp["result"]);
        });
        this.backend.getPage('payment').subscribe(function (resp) {
            _this.page = JSON.parse(resp["result"]);
        });
    }
    PaymentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-payment',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/payment/payment.page.html"*/'<ion-content class="static-page payment-page yellow" no-bounce>\n	<nav-header></nav-header>\n	<div class="static-page__wrap" *ngIf="page">\n		<div class="static-page__left" animate-image>\n			<div class="static-page__image" [style.background-image]="\'url(\' + page.image_url + \')\'"></div>\n		</div>\n		<div class="static-page__right" animate-aside>\n			<h1 #title class="static-page__title yellow">{{page.name}}</h1>\n			<div #description class="static-page__description" [innerHTML]="page.description">\n			</div>\n\n			<ul #list class="static-page__list" *ngIf="payments.length > 0">\n				<li class="static-page__list-item" *ngFor="let payment of payments">\n					<div class="static-page__list-item-image">\n						<img [src]="payment.image_url">\n					</div>\n					<div class="static-page__list-item-info">\n						<div class="static-page__list-item-title">{{payment.name}}</div>\n						<div class="static-page__list-item-description">\n							{{payment.description}}\n						</div>\n					</div>\n				</li>\n			</ul>\n\n			<img #illustartion class="static-page__illustration" [src]="page.illustration_url">\n			<nav-footer></nav-footer>\n		</div>\n	</div>\n</ion-content>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/payment/payment.page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__shared_backend_backend_service__["a" /* BackendService */]])
    ], PaymentPage);
    return PaymentPage;
}());

//# sourceMappingURL=payment.page.js.map

/***/ }),

/***/ 700:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnimateAsideDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_animejs__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_animejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_animejs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AnimateAsideDirective = /** @class */ (function () {
    function AnimateAsideDirective(elementRef) {
        this.elementRef = elementRef;
        this.duration = 600;
    }
    AnimateAsideDirective.prototype.ngOnInit = function () {
        //this.animate();
    };
    AnimateAsideDirective.prototype.animate = function () {
        var animate = __WEBPACK_IMPORTED_MODULE_1_animejs__["timeline"]();
        animate.add(this.aside());
        //.add(this.animateText());
        if (this.list) {
            //animate.add(this.animateList());
        }
        if (this.illustartion) {
            //animate.add(this.animateIllustration());
        }
    };
    AnimateAsideDirective.prototype.animateIllustration = function () {
        var illustration = this.illustartion.nativeElement;
        return {
            targets: illustration,
            opacity: 1,
            translateY: [
                { value: '100%', duration: 0 },
                { value: 0, duration: 3 * this.duration }
            ],
            easing: 'easeOutExpo'
        };
    };
    AnimateAsideDirective.prototype.animateList = function () {
        var _this = this;
        var list = this.list.nativeElement.querySelectorAll('.static-page__list-item');
        return {
            targets: list,
            opacity: 1,
            easing: 'easeOutExpo',
            translateX: [
                { value: '100%', duration: 0 },
                { value: 0, duration: this.duration }
            ],
            duration: this.duration,
            delay: function (el, i, l) {
                return (i * _this.duration);
            }
        };
    };
    AnimateAsideDirective.prototype.animateText = function () {
        var _this = this;
        return {
            targets: [
                this.title.nativeElement,
                this.description.nativeElement
            ],
            opacity: 1,
            translateX: [
                { value: '100%', duration: 0 },
                { value: 0, duration: this.duration }
            ],
            easing: 'easeOutExpo',
            delay: function (el, i, l) {
                return (i * _this.duration);
            }
        };
    };
    AnimateAsideDirective.prototype.aside = function () {
        return {
            targets: this.elementRef.nativeElement,
            easing: 'linear',
            translateX: [
                { value: '100%', duration: 0 },
                { value: 0, duration: this.duration }
            ]
        };
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* ContentChild */])('illustartion'),
        __metadata("design:type", Object)
    ], AnimateAsideDirective.prototype, "illustartion", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* ContentChild */])('list'),
        __metadata("design:type", Object)
    ], AnimateAsideDirective.prototype, "list", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* ContentChild */])('title'),
        __metadata("design:type", Object)
    ], AnimateAsideDirective.prototype, "title", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["q" /* ContentChild */])('description'),
        __metadata("design:type", Object)
    ], AnimateAsideDirective.prototype, "description", void 0);
    AnimateAsideDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: "[animate-aside]",
            exportAs: "animate-aside"
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], AnimateAsideDirective);
    return AnimateAsideDirective;
}());

//# sourceMappingURL=animate-aside.directive.js.map

/***/ }),

/***/ 701:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnimateImageDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_animejs__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_animejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_animejs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AnimateImageDirective = /** @class */ (function () {
    function AnimateImageDirective(elementRef) {
        this.elementRef = elementRef;
        this.duration = 3000;
    }
    AnimateImageDirective.prototype.ngOnInit = function () {
        //this.animate();
    };
    AnimateImageDirective.prototype.animate = function () {
        __WEBPACK_IMPORTED_MODULE_1_animejs__({
            targets: this.elementRef.nativeElement,
            opacity: 1,
            translateX: [
                { value: '100%', duration: 0 },
                { value: 0, duration: this.duration }
            ],
            easing: 'easeOutExpo'
        });
    };
    AnimateImageDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: "[animate-image]",
            exportAs: "animate-image"
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], AnimateImageDirective);
    return AnimateImageDirective;
}());

//# sourceMappingURL=animate-image.directive.js.map

/***/ }),

/***/ 702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__basket_service__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var BasketDirective = /** @class */ (function () {
    function BasketDirective(elementRef, basket) {
        this.elementRef = elementRef;
        this.basket = basket;
        this.id = null;
    }
    BasketDirective.prototype.onClick = function (event) {
        this.basket.add(this.id);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('add-to-basket'),
        __metadata("design:type", Number)
    ], BasketDirective.prototype, "id", void 0);
    BasketDirective = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: "[add-to-basket]",
            host: {
                "(click)": "onClick($event)"
            }
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */], __WEBPACK_IMPORTED_MODULE_1__basket_service__["a" /* BasketService */]])
    ], BasketDirective);
    return BasketDirective;
}());

//# sourceMappingURL=basket.directive.js.map

/***/ }),

/***/ 703:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketBadgeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__basket_service__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__nav_nav_service__ = __webpack_require__(27);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BasketBadgeComponent = /** @class */ (function () {
    function BasketBadgeComponent(basket, nav) {
        this.basket = basket;
        this.nav = nav;
    }
    BasketBadgeComponent.prototype.getCount = function () {
        return this.basket.getCount();
    };
    BasketBadgeComponent.prototype.openOrder = function () {
        this.nav.goTo('order', {});
    };
    BasketBadgeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'basket-badge',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/basket/basket-badge/basket-badge.component.html"*/'<div class="basket-badge" *ngIf="getCount() > 0" (click)="openOrder()">\n	<div class="basket-badge__count">{{getCount()}}</div>\n</div>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/basket/basket-badge/basket-badge.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__basket_service__["a" /* BasketService */], __WEBPACK_IMPORTED_MODULE_2__nav_nav_service__["a" /* NavService */]])
    ], BasketBadgeComponent);
    return BasketBadgeComponent;
}());

//# sourceMappingURL=basket-badge.component.js.map

/***/ }),

/***/ 704:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasketOrderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__basket_service__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__menu_menu_data__ = __webpack_require__(356);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__nav_nav_service__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__event_event_enum__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var BasketOrderComponent = /** @class */ (function () {
    function BasketOrderComponent(nav, events, basket) {
        var _this = this;
        this.nav = nav;
        this.events = events;
        this.basket = basket;
        this.sum = 0;
        this.sumChange = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.products = [];
        this.basketProducts = null;
        this.count = 3;
        this.menuItems = __WEBPACK_IMPORTED_MODULE_3__menu_menu_data__["b" /* MenuItemsForOrder */];
        this.basketProducts = this.basket.get();
        this.basket.getProducts().subscribe(function (products) {
            _this.products = products;
            _this.updateSum();
        });
        this.events.subscribe(__WEBPACK_IMPORTED_MODULE_5__event_event_enum__["a" /* EventTopic */].BasketCleaned, function () {
            _this.basket.getProducts().subscribe(function (products) {
                _this.products = products;
                _this.updateSum();
            });
        });
    }
    BasketOrderComponent.prototype.ngOnDestroy = function () {
        this.events.unsubscribe(__WEBPACK_IMPORTED_MODULE_5__event_event_enum__["a" /* EventTopic */].BasketCleaned);
    };
    BasketOrderComponent.prototype.getCount = function () {
        return this.basket.getCount();
    };
    BasketOrderComponent.prototype.decrement = function (id) {
        this.basketProducts[id]--;
        if (this.basketProducts[id] === 0) {
            this.basketProducts[id] = 1;
        }
        this.save();
    };
    BasketOrderComponent.prototype.increment = function (id) {
        this.basketProducts[id]++;
        this.save();
    };
    BasketOrderComponent.prototype.remove = function (index) {
        delete this.basketProducts[this.products[index].id];
        this.products.splice(index, 1);
        if (this.products.length === 1) {
            this.slides.slideTo(0);
        }
        this.save();
    };
    BasketOrderComponent.prototype.next = function () {
        this.slides.slideNext();
    };
    BasketOrderComponent.prototype.prev = function () {
        this.slides.slidePrev();
    };
    BasketOrderComponent.prototype.goTo = function (page, category) {
        this.nav.goTo(page, { category: category, subcategory: 'all' });
    };
    BasketOrderComponent.prototype.save = function () {
        this.basket.set(this.basketProducts);
        this.updateSum();
    };
    BasketOrderComponent.prototype.updateSum = function () {
        this.sum = this.basket.getSum(this.products);
        this.sumChange.emit(this.sum);
    };
    BasketOrderComponent.prototype.resizeSlider = function () {
        var width = window.innerWidth;
        if (width <= 1024) {
            this.count = 1;
            return;
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('Slides'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* Slides */])
    ], BasketOrderComponent.prototype, "slides", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('sum'),
        __metadata("design:type", Number)
    ], BasketOrderComponent.prototype, "sum", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", Object)
    ], BasketOrderComponent.prototype, "sumChange", void 0);
    BasketOrderComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'basket-order',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/basket/basket-order/basket-order.component.html"*/'<div class="basket-order">\n	<h1 class="basket-order__title" *ngIf="products.length > 0">Товары</h1>\n	<div class="basket-order__slider" [class.basket-order__slider--empty]="products.length === 0">\n		<ion-slides [slidesPerView]="count" direction="vertical" #Slides>\n		  <ion-slide *ngFor="let product of products; index as i">\n		  	<div class="basket-order__item">\n		  		<div class="basket-order__item-image" [style.background-image]="\'url(\' + product.image_url + \')\'"></div>\n		  		<div class="basket-order__item-info">\n		  			<div class="basket-order__item-title">{{product.title}}</div>\n		  			<div class="basket-order__item-calc">\n		  				<div class="basket-order__item-input">\n		  					<div class="basket-order__item-dec" (click)="decrement(product.id)" [class.disabled]="basketProducts[product.id] === 1"></div>\n		  					<div class="basket-order__item-count">{{basketProducts[product.id]}}</div>\n		  					<div class="basket-order__item-inc" (click)="increment(product.id)"></div>\n		  				</div>\n		  				<div class="basket-order__item-price">{{(product.price * basketProducts[product.id]) | currency}}</div>\n		  				<div class="basket-order__item-remove" (click)="remove(i)"></div>\n		  			</div>\n		  		</div>\n		  	</div>\n		  </ion-slide>\n		</ion-slides>\n		<div *ngIf="slides">\n			<div class="arrow-up" (click)="prev()" *ngIf="!slides.isBeginning()">\n				<img src="assets/images/icons/arrow-yellow.svg">\n			</div>\n			<div class="arrow-down" (click)="next()" *ngIf="!slides.isEnd()">\n				<img src="assets/images/icons/arrow-yellow.svg">\n			</div>\n		</div>\n	</div>\n\n	<h2 class="basket-order__sum" *ngIf="products.length > 0">Итого к оплате: {{sum | currency}}</h2>\n	<div *ngIf="products.length === 0" class="basket-order__empty">\n		<h2 class="white">В вашей корзине пока нет товаров!</h2>\n		<h3 class="white">Но не расстраивайтесь, у нас очень большой выбор:</h3>\n\n		<div class="basket-order__menu">\n			<div class="basket-order__menu-item" *ngFor="let item of menuItems" (click)="goTo(item.page, item.category)">\n				<div class="basket-order__menu-icon">\n					<img src="assets/images/icons/{{item.icon}}.svg">\n				</div>\n				<div class="basket-order__menu-title">{{item.name}}</div>\n			</div>\n		</div>\n	</div>\n\n</div>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/shared/basket/basket-order/basket-order.component.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__nav_nav_service__["a" /* NavService */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* Events */],
            __WEBPACK_IMPORTED_MODULE_1__basket_service__["a" /* BasketService */]])
    ], BasketOrderComponent);
    return BasketOrderComponent;
}());

//# sourceMappingURL=basket-order.component.js.map

/***/ }),

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PricePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PricePipe = /** @class */ (function () {
    function PricePipe() {
    }
    PricePipe.prototype.transform = function (value, options) {
        return value + ' грн.';
    };
    PricePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: "currency",
            pure: false
        }),
        __metadata("design:paramtypes", [])
    ], PricePipe);
    return PricePipe;
}());

//# sourceMappingURL=price.pipe.js.map

/***/ }),

/***/ 706:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(26);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_model__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__environments_environments__ = __webpack_require__(45);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__shared_basket_basket_modal_basket_modal_component__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__shared_user_user_service__ = __webpack_require__(155);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__shared_basket_basket_service__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__shared_backend_backend_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var OrderPage = /** @class */ (function () {
    function OrderPage(toastCtrl, formBuilder, userService, basket, backend, modalCtrl) {
        var _this = this;
        this.toastCtrl = toastCtrl;
        this.formBuilder = formBuilder;
        this.userService = userService;
        this.basket = basket;
        this.backend = backend;
        this.modalCtrl = modalCtrl;
        this.env = __WEBPACK_IMPORTED_MODULE_4__environments_environments__["a" /* environment */];
        this.sum = 0;
        this.userOrder = new __WEBPACK_IMPORTED_MODULE_3__order_model__["a" /* OrderModel */]();
        this.deliveries = [];
        this.payments = [];
        this.isFormLoaded = false;
        this.userOrder = this.userService.get();
        this.backend.getDeliveries().subscribe(function (resp) {
            _this.deliveries = JSON.parse(resp["result"]);
            if (!_this.userOrder.delivery) {
                _this.userOrder.delivery = _this.deliveries[0];
            }
            _this.backend.getPayments().subscribe(function (resp) {
                _this.payments = JSON.parse(resp["result"]);
                if (!_this.userOrder.payment) {
                    _this.userOrder.payment = _this.deliveries[0];
                }
                _this.buildForm(_this.userOrder);
                _this.isFormLoaded = true;
            });
        });
    }
    OrderPage.prototype.onSubmit = function () {
        if (this.orderForm.invalid) {
            this.presentToast('Пожалуйста заполните все поля!');
            return;
        }
        this.showConfirmModal();
    };
    OrderPage.prototype.getDelivery = function () {
        var _this = this;
        return this.deliveries.filter(function (delivery) { return delivery.id === _this.orderForm.controls.delivery.value; })[0];
    };
    OrderPage.prototype.changeDelivery = function () {
        var delivery = this.getDelivery();
        if (delivery.postal && !this.basket.enableDelivery()) {
            this.presentToast('Данный тип доставки невозможен, так как в корзине присутствуют товары, которые не доставляются новой почтой.');
            this.orderForm.controls.delivery.setValue(2);
        }
        this.updateForm(delivery);
    };
    OrderPage.prototype.showConfirmModal = function () {
        var confirmModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__shared_basket_basket_modal_basket_modal_component__["a" /* BasketModalComponent */], this.orderForm.value);
        confirmModal.present();
        confirmModal.onDidDismiss(function (confirm) {
            if (!confirm)
                return;
            //this.sum = 0;
        });
    };
    OrderPage.prototype.buildForm = function (order) {
        this.orderForm = this.formBuilder.group(this.getBasicForm(order));
        this.updateForm(this.getDelivery());
    };
    OrderPage.prototype.updateForm = function (delivery) {
        if (delivery.postal === true) {
            this.orderForm.removeControl('address');
            this.orderForm.addControl('city', new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](this.userOrder.deliveryAddress.city, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required));
            this.orderForm.addControl('department', new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](this.userOrder.deliveryAddress.department, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required));
            return;
        }
        if (delivery.addres === true) {
            this.orderForm.removeControl('department');
            this.orderForm.addControl('city', new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](this.userOrder.deliveryAddress.city, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required));
            this.orderForm.addControl('address', new __WEBPACK_IMPORTED_MODULE_2__angular_forms__["b" /* FormControl */](this.userOrder.deliveryAddress.address, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required));
            return;
        }
        this.orderForm.removeControl('department');
        this.orderForm.removeControl('address');
        this.orderForm.removeControl('city');
    };
    OrderPage.prototype.getBasicForm = function (order) {
        return {
            email: [
                order.email,
                [__WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].email]
            ],
            fio: [
                order.fio,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
            ],
            phone: [
                order.phone,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
            ],
            delivery: [
                order.delivery.id,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
            ],
            payment: [
                order.payment.id,
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["g" /* Validators */].required
            ]
        };
    };
    OrderPage.prototype.presentToast = function (message) {
        var toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'bottom'
        });
        toast.present();
    };
    OrderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-order',template:/*ion-inline-start:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/order/order.page.html"*/'<ion-content class="static-page order-page yellow" no-bounce>\n	<nav-header></nav-header>\n	<div class="static-page__wrap">\n		<div class="static-page__left" animate-image>\n			<basket-order [(sum)]="sum"></basket-order>\n		</div>\n		<div class="static-page__right" animate-aside>\n			<h1 class="static-page__title yellow">Корзина</h1>\n			<div class="static-page__description">\n				Заполните контактную информацию чтобы мы могли с вами связаться для уточнения заказа\n			</div>\n 			<form (ngSubmit)="onSubmit()" [formGroup]="orderForm" class="form order-page__form" *ngIf="isFormLoaded">\n				<div class="order-page__form-fields">\n					<ion-item>\n						<ion-label color="primary">ФИО:</ion-label>\n	    				<ion-input formControlName="fio"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label color="primary">Телефон:</ion-label>\n	    				<ion-input formControlName="phone"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label color="primary">Email:</ion-label>\n	    				<ion-input formControlName="email"></ion-input>\n					</ion-item>\n				</div>\n				<h3>Выберите способ доставки:</h3>\n				<ion-list radio-group formControlName="delivery" class="order-page__radio" (ionChange)="changeDelivery()">\n					<ion-item *ngFor="let delivery of deliveries">\n					    <ion-label>\n					    	<img [src]="delivery.image_url">\n					    	{{delivery.name}}\n					    </ion-label>\n					    <ion-radio [value]="delivery.id"></ion-radio>\n					 </ion-item>\n				</ion-list>\n\n				<div *ngIf="getDelivery().postal" class="order-page__form-fields">\n					<ion-item>\n						<ion-label color="primary">Город:</ion-label>\n	    				<ion-input formControlName="city"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label color="primary">№ отделения:</ion-label>\n	    				<ion-input type="number" formControlName="department"></ion-input>\n					</ion-item>\n				</div>\n				<div *ngIf="getDelivery().link" class="static-page__description">\n					Мы всегда рады видеть вас в нашем магазине <br>\n					<a [href]="env.address.link" target="_blank">{{env.address.title}}</a>\n				</div>\n				<div *ngIf="getDelivery().addres" class="order-page__form-fields">\n					<ion-item>\n						<ion-label color="primary">Город:</ion-label>\n	    				<ion-input formControlName="city"></ion-input>\n					</ion-item>\n					<ion-item>\n						<ion-label color="primary">Адресс:</ion-label>\n	    				<ion-input formControlName="address"></ion-input>\n					</ion-item>\n				</div>\n				<h3>Выберите способ оплаты:</h3>\n				<ion-list radio-group formControlName="payment" class="order-page__radio">\n					<ion-item *ngFor="let payment of payments">\n					    <ion-label>\n					    	<img [src]="payment.image_url">\n					    	{{payment.name}}\n					    </ion-label>\n					    <ion-radio [value]="payment.id"></ion-radio>\n					 </ion-item>\n				</ion-list>\n\n				<button ion-button color="primary" type="submit" round class="blue" \n				[disabled]="sum === 0">Оформить</button>\n			</form>\n		</div>\n	</div>\n</ion-content>'/*ion-inline-end:"/Users/olgagolub/Work/pozitiff/frontend/src/pages/order/order.page.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_6__shared_user_user_service__["a" /* UserService */],
            __WEBPACK_IMPORTED_MODULE_7__shared_basket_basket_service__["a" /* BasketService */],
            __WEBPACK_IMPORTED_MODULE_8__shared_backend_backend_service__["a" /* BackendService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* ModalController */]])
    ], OrderPage);
    return OrderPage;
}());

//# sourceMappingURL=order.page.js.map

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventTopic; });
var EventTopic;
(function (EventTopic) {
    EventTopic["OpenProduct"] = "product:open";
    EventTopic["BasketCleaned"] = "basket:cleaned";
})(EventTopic || (EventTopic = {}));
//# sourceMappingURL=event.enum.js.map

/***/ })

},[359]);
//# sourceMappingURL=main.js.map